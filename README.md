# README #

Wiki
[https://bitbucket.org/gregorychapman/linegraph/wiki/Home](https://bitbucket.org/gregorychapman/linegraph/wiki/Home)

LineGraph is a Java-based line graph library that targets the Android and Java desktop. Currently, Android is supported with plans to move to Java desktop.

### What is this repository for? ###

* This repository is the basis for LineGraph tool which displays x,y data on a graph with a fast update rate. This functionality is similar to an oscilloscope display.
* Libraries provided are basis for your application. There is a demo to show how to use the LineGraph and feed it data.
* Android currently supported
* Java desktop coming soon

### How do I get set up? ###

* Install Android Studio (IntelliJ version)
* Need Android v16+ on device

### Who do I talk to? ###

* Greg Chapman