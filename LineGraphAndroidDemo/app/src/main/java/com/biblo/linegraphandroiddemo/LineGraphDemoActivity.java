package com.biblo.linegraphandroiddemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.WritablePoint;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;
import com.biblo.datastreamviewer.linegraph.model.LineGraphModel;
import com.biblo.datastreamviewer.linegraph.model.MultiPointLine;
import com.biblo.datastreamviewer.linegraph.model.SimpleLineGraphModel;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.presenter.ThreadSafeLineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.view.android.LineGraphSurfaceView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author gchapman
 * Contains the application to embed the line graph
 */
public class LineGraphDemoActivity extends Activity {

    private static final String TAG = LineGraphDemoActivity.class.getSimpleName();
    private static final int TITLE_COLOR = 0xFFFFC800;
    private static final int TITLE_SIZE = 35;
    private static final String TITLE_FONT = "sans";
    private static final int TITLE_Y_OFFSET = 150;
    private static final int DATA_LINE_COLOR = 0xFFFF0000;
    private static final float DATA_LINE_WIDTH = 1.5f;

    private LineGraphSurfaceView mLineGraphView;
    private LineGraphModel mLineGraphModel;
    private LineGraphPresenter mLineGraphPresenter;
    private MultiPointLine mLine;
    private Timer mTimer;
    private WritablePoint mTitleLocation;
    private LineGraphPresenter.TextProperties mTitleProperties;
    private AttributableText mTitle;
    private boolean mTitleLocationSet;
    private WritableRectangle mDisplayPort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDisplayPort = new WritableRectangle();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //keep screen on
        setContentView(R.layout.activity_line_graph_demo);
        RelativeLayout frame = (RelativeLayout) findViewById(R.id.frame);
        createLineGraph();
        frame.addView(mLineGraphView);
    }

    private void createLineGraph() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        mLineGraphView = new LineGraphSurfaceView(this, metrics);
        mLineGraphModel = new SimpleLineGraphModel();
        mLineGraphPresenter = new ThreadSafeLineGraphPresenter(mLineGraphModel, mLineGraphView);
        mLineGraphView.setPresenter(mLineGraphPresenter);

        mLine = mLineGraphPresenter.addDataLine(new LineGraphPresenter.LineProperties(
                DATA_LINE_COLOR, DATA_LINE_WIDTH)); //single red line

        mTitleLocation = new WritablePoint();
        mTitleProperties =
                new LineGraphPresenter.TextProperties(TITLE_COLOR, TITLE_SIZE, TITLE_FONT);
        mTitleProperties.centered = true;
        mTitle = mLineGraphPresenter.addText("", mTitleLocation, mTitleProperties);
    }

    private void setupTitleLocation() {
        if(mTitleLocationSet) {
            return;
        }
        mTitleLocationSet = true;
        mLineGraphPresenter.getDisplayPort(mDisplayPort);
        mTitleLocation.setX(mDisplayPort.getLeft() + mDisplayPort.getWidth() / 2);
        mTitleLocation.setY(mDisplayPort.getTop() - TITLE_Y_OFFSET);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLineGraphPresenter.start();
        final float[] data = new float[2000];
        final Random random = new Random();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                for(int i = 0; i < data.length; ++i) {
                    data[i] = random.nextFloat() * 100;
                }
                mLineGraphPresenter.onData(mLine, data);

                setupTitleLocation();
                mLineGraphPresenter.onText(mTitle, "Random", mTitleLocation, mTitleProperties);
            }
        }, 10, 10);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLineGraphPresenter.stop();
        mTimer.cancel();
        mTimer = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.line_graph_demo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(id) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }
}
