package com.biblo.linegraphandroiddemo;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import com.biblo.datastreamviewer.linegraph.view.android.BaseSettingsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends BaseSettingsActivity {

    @Override
    protected void bindPreferenceSummaries() {
        Resources resources = getResources();
        bindPreferenceSummaryToValue(findPreference(resources.getString(R.string.key_axis_minX)));
    }

    @Override
    protected List<Integer> getPreferences() {
        List<Integer> prefs = new ArrayList<Integer>();
        prefs.add(R.xml.pref_axis);
        return prefs;
    }

    @Override
    protected List<String> getFragmentNames() {
        List<String> frags = new ArrayList<String>();
        frags.add(FeaturePreferenceFragment.class.getName());
        return frags;
    }

    @Override
    protected List<Integer> getHeaders() {
        List<Integer> headers = new ArrayList<Integer>();
        headers.add(R.xml.pref_headers);
        return headers;
    }

    /**
     * This fragment shows data and sync preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class FeaturePreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_axis);

            bindPreferenceSummaryToValue(findPreference("key_axis_minX"));
        }
    }
}
