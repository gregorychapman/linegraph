package com.biblo.datastreamviewer.common.model.basic;

import java.util.HashMap;

/**
 * @author gchapman
 * Object that has some extensible properties associated with it.
 */
public class PropertyObject {

    private HashMap<Integer, Object> mProperties;
    private boolean mCachedPropertyValid;
    private int mCachedProperty;
    private Object mCachedPropertyObject;

    /**
     * Contains a property map
     */
    public PropertyObject() {
        mProperties = new HashMap<Integer, Object>();
        mCachedPropertyValid = false;
    }

    /**
     * Assigns property
     * @param property Key for property
     * @param propertyObject Object value for property
     */
    public void putProperty(int property, Object propertyObject) {
        mProperties.put(property, propertyObject);
        //update cache
        if(mCachedPropertyValid && mCachedProperty == property) {
            mCachedPropertyObject = propertyObject;
        }
    }

    /**
     * Gets a property base on key
     * @param property Key for property
     * @return Object value for property
     */
    public Object getProperty(int property) {
        if(mCachedPropertyValid && mCachedProperty == property) {
            return mCachedPropertyObject;
        }

        //update cache
        mCachedPropertyValid = true;
        mCachedProperty = property;
        mCachedPropertyObject = mProperties.get(property);
        return mCachedPropertyObject;
    }

    /**
     * Gets a property base on key or use default and cache it
     * @param property Key for property
     * @return Object value for property
     */
    public Object getProperty(int property, Object defaultPropertyObject) {
        Object propertyObject = getProperty(property);
        if(propertyObject != null) {
            return propertyObject;
        }
        putProperty(property, defaultPropertyObject);
        return getProperty(property);
    }

    /**
     * Removes a property
     * @param property Key for property
     */
    public void removeProperty(int property) {
        mProperties.remove(property);
        //update cache
        if(mCachedPropertyValid && mCachedProperty == property) {
            mCachedPropertyObject = null;
            mCachedPropertyValid = false;
        }
    }
}
