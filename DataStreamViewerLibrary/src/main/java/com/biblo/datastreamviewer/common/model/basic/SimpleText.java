package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Represents some text at a starting point.
 * Point is at the bottom left of the text
 */
public class SimpleText {

    protected String mText;
    protected Point mPoint;

    /**
     * Default 0,0 and ""
     */
    public SimpleText() {
        mText = "";
        mPoint = new Point();
    }

    /**
     * @return String of text
     */
    public final String getText() {
        return mText;
    }

    /**
     * @return Bottom left position
     */
    public final Point getPoint() {
        return mPoint;
    }
}
