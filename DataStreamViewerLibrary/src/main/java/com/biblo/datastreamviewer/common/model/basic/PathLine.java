package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Interface for line paths
 */
public interface PathLine {
    /**
     * Will clear the content of path, equal or more thorough than reset.
     */
    void clear();

    /**
     * Quick reset of the path
     */
    void reset();

    /**
     * Add new point to the path
     * @param x X value
     * @param y Y value
     */
    void add(float x, float y);

    /**
     * Draws the path
     */
    void draw();
}
