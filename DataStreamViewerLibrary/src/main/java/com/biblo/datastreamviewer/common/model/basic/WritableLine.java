package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Represents a simple (2 pt) line with properties, modifiable
 */
public class WritableLine extends SimpleLine {

    /**
     * Writable simple line
     */
    public WritableLine() {
        mStart = new WritablePoint();
        mEnd = new WritablePoint();
    }

    /**
     * Writable simple line
     * @param start Start point
     * @param end End point
     */
    public WritableLine(Point start, Point end) {
        mStart = new WritablePoint(start.getX(), start.getY());
        mEnd = new WritablePoint(end.getX(), end.getY());
    }

    /**
     * Will set the start point
     * @param start Start point
     */
    public void setStart(Point start) {
        ((WritablePoint)mStart).set(start);
    }

    /**
     * Will set the start point
     * @param x Start point x
     * @param y Start point y
     */
    public void setStart(float x, float y) {
        WritablePoint start = ((WritablePoint)mStart);
        start.setX(x);
        start.setY(y);
    }

    /**
     * Will set the end point
     * @param end End point
     */
    public void setEnd(Point end) {
        ((WritablePoint)mEnd).set(end);
    }

    /**
     * Will set the end point
     * @param x End point x
     * @param y End point y
     */
    public void setEnd(float x, float y) {
        WritablePoint end = ((WritablePoint)mEnd);
        end.setX(x);
        end.setY(y);
    }
}
