package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Modifiable point data
 */
public class WritablePoint extends Point {

    public WritablePoint() {
        super();
    }

    public WritablePoint(float x, float y) {
        super(x, y);
    }

    public void setX(float x){
        mX = x;
    }

    public void setY(float y){
        mY = y;
    }

    public void addX(float x){
        mX += x;
    }

    public void addY(float y){
        mY += y;
    }

    public void set(Point point) {
        mX = point.mX;
        mY = point.mY;
    }
}
