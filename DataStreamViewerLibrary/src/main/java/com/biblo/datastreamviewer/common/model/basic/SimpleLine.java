package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Represents a simple (2 pt) line
 */
public class SimpleLine {

    protected Point mStart;
    protected Point mEnd;

    /**
     * Simple 2 point line
     */
    public SimpleLine() {
        mStart = new Point();
        mEnd = new Point();
    }

    /**
     * @return Start point
     */
    public final Point getStart() {
        return mStart;
    }

    /**
     * @return End point
     */
    public final Point getEnd() {
        return mEnd;
    }

}
