package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Represents rectangle shape, origin at left bottom
 */
public class Rectangle {

    protected WritablePoint mLeftBottom;
    protected WritablePoint mRightTop;
    protected float mWidth;
    protected float mHeight;

    /**
     * Constructs default rectangle. 0,0,0,0
     */
    public Rectangle() {
        mLeftBottom = new WritablePoint();
        mRightTop = new WritablePoint();
        mWidth = 0;
        mHeight = 0;
    }

    /**
     * Constructs rectangle with dimensions
     * @param x Bottom left X coordinate
     * @param y Bottom left Y coordinate
     * @param width Width of rectangle
     * @param height Height of rectangle
     */
    public Rectangle(float x, float y, float width, float height) {
        mLeftBottom = new WritablePoint(x, y);
        mRightTop = new WritablePoint(x+width, y+height);
        mWidth = width;
        mHeight = height;
    }

    /**
     * @return Left X of rectangle
     */
    public float getLeft(){
        return mLeftBottom.getX();
    }

    /**
     * @return Right X of rectangle
     */
    public float getRight(){
        return mRightTop.getX();
    }

    /**
     * @return Top Y of rectangle
     */
    public float getTop(){
        return mRightTop.getY();
    }

    /**
     * @return Bottom Y of rectangle
     */
    public float getBottom(){
        return mLeftBottom.getY();
    }

    /**
     * @return Width of rectangle
     */
    public float getWidth() {
        return mWidth;
    }

    /**
     * @return Height of rectangle
     */
    public float getHeight() {
        return mHeight;
    }

    /**
     * Check for x,y in rectangle
     * @param x X coordinate
     * @param y Y coordinate
     * @return True if point in rectangle
     */
    public boolean contains(float x, float y) {
        return mLeftBottom.getX() < x && x < mRightTop.getX()
                && mLeftBottom.getY() < y && y < mRightTop.getY();
    }

    /**
     * Checks if the rectangle is within bounds of this rectangle.
     * @param rectangle Rectangle to check for inside this rectangle
     * @return True if the rectangle is inside this
     */
    public boolean contains(Rectangle rectangle) {
        return getLeft() <= rectangle.getLeft() &&
               getRight() >= rectangle.getRight() &&
               getBottom() <= rectangle.getBottom() &&
               getTop() >= rectangle.getTop();
    }

    /**
     * Will contain the rectangle in the bounds of this rectangle.
     * @param rectangle Rectangle to bound.
     */
    public void contain(WritableRectangle rectangle) {
        float left = rectangle.getLeft();
        float right = rectangle.getRight();
        float top = rectangle.getTop();
        float bottom = rectangle.getBottom();

        //Boundary correction
        if(left < getLeft()) {
            left = getLeft();
            right = left + rectangle.getWidth();
        }
        if(right > getRight()) {
            right = getRight();
            left = right - rectangle.getWidth();
        }
        if(top > getTop()) {
            top = getTop();
            bottom = top - rectangle.getHeight();
        }
        if(bottom < getBottom()) {
            bottom = getBottom();
            top = bottom + rectangle.getHeight();
        }

        //Dimension correction
        if(bottom < getBottom() || top > getTop()) {
            bottom = getBottom();
            top = getTop();
        }
        if(right > getRight() || left < getLeft()) {
            right = getRight();
            left = getLeft();
        }

        rectangle.set(left, top, right, bottom);
    }

    @Override
    public String toString() {
        return "left:" + mLeftBottom.getX() + ", " +
               "bottom:" + mLeftBottom.getY() + ", " +
               "right:" + mRightTop.getX() + ", " +
               "top:" + mRightTop.getY();
    }

    /**
     * Checks values of this rectangle and another for equality
     * @param rectangle Rectangle to check
     * @return True if equal
     */
    public boolean equals(Rectangle rectangle) {
        return rectangle.getLeft() == getLeft() &&
               rectangle.getRight() == getRight() &&
               rectangle.getBottom() == getBottom() &&
               rectangle.getTop() == getTop();
    }

    /**
     * Checks the dimensions of this rectangle and another for equality
     * @param rectangle Rectangle to check
     * @return True if equal
     */
    public boolean dimensionsEqual(Rectangle rectangle) {
        return rectangle.getWidth() == getWidth() &&
               rectangle.getHeight() == getHeight();
    }

    /**
     * @return True if the width, height and points are valid floats (not NaN or infinity)
     */
    public boolean isValid() {
        return mLeftBottom.isValid() && mRightTop.isValid();
    }
}
