package com.biblo.datastreamviewer.common.model;

import com.biblo.datastreamviewer.common.model.basic.Rectangle;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;

/**
 * @author gchapman
 * Audit the viewport with the world boundary. Corrects the viewport if out of bounds.
 */
public class WorldAuditor {

    private Rectangle mWorldBounds;

    /**
     * Will watch the world bounds for the viewport
     * @param worldBounds Boundary to watch
     */
    public WorldAuditor(Rectangle worldBounds) {
        mWorldBounds = worldBounds;
    }

    /**
     * Will update the viewport and check for validity in the world
     * @param viewPort Viewport to check for updates with
     * @return True if update is ok, false if correction made
     */
    public boolean update(WritableRectangle viewPort) {
        if(mWorldBounds.contains(viewPort)) {
            return true;
        }
        mWorldBounds.contain(viewPort);
        return false;
    }
}
