package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Represents a point in 2d: x, y
 */
public class Point {

    protected float mX;
    protected float mY;

    /**
     * Default is 0,0
     */
    public Point() {
        mX = 0;
        mY = 0;
    }

    /**
     * Constructs using params
     * @param x X coordinate
     * @param y Y coordinate
     */
    public Point(float x, float y) {
        mX = x;
        mY = y;
    }

    /**
     * @return X coordinate
     */
    public float getX(){
        return mX;
    }

    /**
     * @return Y coordinate
     */
    public float getY(){
        return mY;
    }

    @Override
    public String toString() {
        return "x:" + mX + ", y:" + mY;
    }

    /**
     * @return True if the width, height and points are valid floats (not NaN or infinity)
     */
    public boolean isValid() {
        return !Float.isInfinite(mX) && !Float.isNaN(mX) && !Float.isInfinite(mY) && !Float.isNaN(mY);
    }
}
