package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Represents modifiable text that has properties.
 */
public class AttributableWritableText extends AttributableText {

    /**
     * Construct text
     */
    public AttributableWritableText() {
        super();
    }

    /**
     * Constructs the text with existing point and string
     * @param text String of text
     * @param point Bottom left point
     */
    public AttributableWritableText(String text, Point point) {
        super(text, point);
    }

    /**
     * Sets the position of text
     * @param point Bottom left point
     */
    public void setPoint(Point point) {
        mWritableText.setPoint(point);
    }

    /**
     * Sets the position of text
     * @param x Bottom left X
     * @param y Bottom left Y
     */
    public void setPoint(float x, float y) {
        mWritableText.setPoint(x, y);
    }

    /**
     * Sets the string of the text
     * @param text String of text
     */
    public void setText(String text) {
        mWritableText.setText(text);
    }
}
