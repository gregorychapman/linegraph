package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Represents text that has properties.
 */
public class AttributableText extends PropertyObject {

    protected WritableText mWritableText;

    /**
     * Construct text
     */
    public AttributableText() {
        mWritableText = new WritableText();
    }

    /**
     * Constructs the text with existing point and string
     * @param text String of text
     * @param point Bottom left point
     */
    public AttributableText(String text, Point point) {
        mWritableText = new WritableText(text, point);
    }

    /**
     * @return Simple text object
     */
    public SimpleText getText() {
        return mWritableText;
    }
}
