package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Represents rectangle shape, origin at left bottom
 */
public class WritableRectangle extends Rectangle {

    /**
     * Default rectangle, 0,0,0,0
     */
    public WritableRectangle() {
        super();
    }

    /**
     * Construct rectangle with params
     * @param x Bottom left X coordinate
     * @param y Bottom left Y coordinate
     * @param width Width of rectangle
     * @param height Height of rectangle
     */
    public WritableRectangle(float x, float y, float width, float height) {
        super(x, y, width, height);
    }

    /**
     * Sets the bottom left x
     * @param x X coordinate
     */
    public void setX(float x){
        mLeftBottom.setX(x);
        mRightTop.setX(x + mWidth);
    }

    /**
     * Sets the bottom left y
     * @param y Y coordinate
     */
    public void setY(float y){
        mLeftBottom.setY(y);
        mRightTop.setY(y + mHeight);
    }

    /**
     * Sets the width of the rectangle
     * @param width Width of the rectangle
     */
    public void setWidth(float width){
        mWidth = width;
        mRightTop.setX(mLeftBottom.getX() + width);
    }

    /**
     * Sets the height of the rectangle
     * @param height Height of the rectangle
     */
    public void setHeight(float height){
        mHeight = height;
        mRightTop.setY(mLeftBottom.getY() + height);
    }

    /**
     * Adds the x value to bottom left point, shift in x
     * @param x Amount to shift
     */
    public void addX(float x){
        setX(this.getLeft()+x);
    }

    /**
     * Adds the y value to bottom left point, shift in y
     * @param y Amount to shift
     */
    public void addY(float y){
        setY(this.getBottom() + y);
    }

    /**
     * Adds to width of rectangle, shifts right while maintaining bottom left
     * @param width Amount to add
     */
    public void addWidth(float width){
        setWidth(getWidth()+width);
    }

    /**
     * Adds to height of rectangle, shifts tpo while maintaining bottom left
     * @param height Amount to add
     */
    public void addHeight(float height){
        setHeight(getHeight() + height);
    }

    /**
     * Will copy the rectangle coordinates/dimensions to this rectangle
     * @param rectangle Rectangle to copy
     */
    public void set(Rectangle rectangle) {
        mLeftBottom.set(rectangle.mLeftBottom);
        mRightTop.set(rectangle.mRightTop);
        mWidth = rectangle.mWidth;
        mHeight = rectangle.mHeight;
    }

    /**
     * Will set the rectangle coordinates/dimensions
     * @param left Left x of rectangle
     * @param top Top y of rectangle
     * @param right Right x of rectangle
     * @param bottom Bottom y of rectangle
     */
    public void set(float left, float top, float right, float bottom) {
        mLeftBottom.setX(left);
        mRightTop.setX(right);
        mRightTop.setY(top);
        mLeftBottom.setY(bottom);
        mWidth = right - left;
        mHeight = top - bottom;
    }
}
