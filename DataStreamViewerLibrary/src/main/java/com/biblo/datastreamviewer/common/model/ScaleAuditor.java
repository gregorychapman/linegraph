package com.biblo.datastreamviewer.common.model;

import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;

/**
 * @author gchapman
 * Audit the viewport checking scale validity
 */
public class ScaleAuditor {

    private final float mMinViewDifference;
    private WritableRectangle mLastValidView;

    /**
     * Will watch the scale for the viewport
     * @param minViewDifference Min difference of view in height or width
     */
    public ScaleAuditor(float minViewDifference) {
        mMinViewDifference = minViewDifference;
        mLastValidView = new WritableRectangle();
    }

    /**
     * Will update the viewport and check for validity in the world
     * @param viewPort Viewport to check for updates with
     * @return True if update is ok, false if correction made
     */
    public boolean update(WritableRectangle viewPort) {
        boolean correction = false;

        if(!viewPort.isValid()) {
            correction = true;
            viewPort.set(mLastValidView);
        }
        if(viewPort.getWidth() < mMinViewDifference) {
            correction = true;
            viewPort.setWidth(mMinViewDifference);
        }
        if(viewPort.getHeight() < mMinViewDifference) {
            correction = true;
            viewPort.setHeight(mMinViewDifference);
        }
        mLastValidView.set(viewPort);
        return !correction;
    }
}
