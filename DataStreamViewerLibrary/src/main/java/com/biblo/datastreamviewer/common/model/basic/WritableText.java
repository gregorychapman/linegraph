package com.biblo.datastreamviewer.common.model.basic;

/**
 * @author gchapman
 * Represents modifiable text
 */
public class WritableText extends SimpleText {

    private WritablePoint mWritablePoint;

    /**
     * Default 0,0 and ""
     */
    public WritableText() {
        super();
        mPoint = mWritablePoint = new WritablePoint();
    }

    /**
     * Constructs the text with existing point and string
     * @param text String of text
     * @param point Bottom left point
     */
    public WritableText(String text, Point point) {
        mText = text;
        mPoint = mWritablePoint = new WritablePoint(point.getX(), point.getY());
    }

    /**
     * Sets the position of text
     * @param point Bottom left point
     */
    public void setPoint(Point point) {
        mWritablePoint.set(point);
    }

    /**
     * Sets the position of text
     * @param x Bottom left X
     * @param y Bottom left Y
     */
    public void setPoint(float x, float y) {
        mWritablePoint.setX(x);
        mWritablePoint.setY(y);
    }

    /**
     * Sets the string of the text
     * @param text String of text
     */
    public void setText(String text) {
        mText = text;
    }
}
