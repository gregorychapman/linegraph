package com.biblo.datastreamviewer.linegraph.model;

import com.biblo.datastreamviewer.common.model.basic.PropertyObject;
import com.biblo.datastreamviewer.common.model.basic.SimpleLine;
import com.biblo.datastreamviewer.common.model.basic.SimpleText;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gchapman
 * This class represents a grid for the display to give context for data
 */
public class Grid extends PropertyObject {

    protected List<SimpleLine> mLines;
    protected List<SimpleText> mText;

    /**
     * No lines by default, derive this
     */
    public Grid() {
        mLines = new ArrayList<SimpleLine>();
        mText = new ArrayList<SimpleText>();
    }

    /**
     * @return SimpleLines to draw representing grid
     */
    public List<SimpleLine> getLines() {
        return mLines;
    }

    /**
     * @return SimpleText to identify the grid lines
     */
    public List<SimpleText> getText() {
        return mText;
    }
}
