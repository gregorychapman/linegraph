package com.biblo.datastreamviewer.linegraph.presenter;

import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.Rectangle;
import com.biblo.datastreamviewer.linegraph.model.LineGraphModel;
import com.biblo.datastreamviewer.linegraph.model.MultiPointLine;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;
import com.biblo.datastreamviewer.linegraph.view.LineGraphView;

import java.util.ArrayList;

/**
 * @author gchapman
 * Interface for the presenter of the line graph MVP
 */
public interface LineGraphPresenter {

    /**
     * Will start the line graph rendering.
     */
    void start();

    /**
     * Will stop the line graph rendering.
     */
    void stop();

    /**
     * Will lock the model to provide thread safety.
     * @return Model of the LineGraph.
     */
    LineGraphModel lockModel();

    /**
     * Unlocks the model to provide thread safety.
     */
    void unlockModel();

    /**
     * Will lock the view to provide thread safety.
     * @return View of the LineGraph.
     */
    LineGraphView lockView();

    /**
     * Unlocks the view to provide thread safety.
     */
    void unlockView();

    /**
     * Set the line properties of the grid.
     * @param properties Line properties of grid (color, width..)
     */
    void setGridProperties(LineProperties properties);

    /**
     * Will add a data line to the model, view with specified properties.
     * @param properties Line properties of data line (color, width..)
     * @return Data line that was added or null if error
     */
    MultiPointLine addDataLine(LineProperties properties);

    /**
     * Will remove a data line from the model, view.
     * @param line Line to remove
     */
    void removeDataLine(MultiPointLine line);

    /**
     * Adds new text object to model
     * @param text String of text
     * @param location Location for the text
     * @param properties Properties of the text
     * @return Text object to use for modification
     */
    AttributableText addText(String text, Point location, TextProperties properties);

    /**
     * Removes the text from the model
     * @param textObject Text object in model
     */
    void removeText(AttributableText textObject);

    /**
     * Clears the line of data
     * @param line Line to clear
     */
    void clear(MultiPointLine line);

    /**
     * Will set the factor for x values. Default is identity (1).
     * @param factorX Scale x values by factorX
     */
    void setDataFactorX(float factorX);

    /**
     * Will update the line with new data.
     * @param line Line to update
     * @param data Data to update line with
     */
    void onData(MultiPointLine line, float[] data);

    /**
     * Will update the line with new data.
     * @param line Line to update
     * @param data Data to update line with
     */
    void onData(MultiPointLine line, ArrayList<Float> data);

    /**
     * Modify the text object
     * @param textObject Object to change
     * @param text String of text
     * @param location Location for the text
     * @param properties Properties of the text
     * @return Text object to use for modification
     */
    void onText(AttributableText textObject, String text, Point location, TextProperties properties);

    /**
     * Gets the display port by writing to out parameter
     * @param mDisplayPort
     */
    void getDisplayPort(WritableRectangle mDisplayPort);

    /**
     * Will get the world boundary which is the max,min the world can scroll to.
     * @param worldBounds World bounds, out param
     */
    void getWorldBounds(WritableRectangle worldBounds);

    /**
     * Will set the world boundary in the model
     * @param worldBounds Boundary for the world.
     */
    void setWorldBounds(Rectangle worldBounds);

    /**
     * Will set the view boundary in the view
     * @param viewPort View of the data
     */
    void setViewPort(Rectangle viewPort);

    /*
     * Events
     */

    /**
     * Event that the user pressed/clicked down on the view.
     * @return True if consumed, false if not
     */
    boolean onDown();

    /**
     * Event that the user double pressed/clicked on the view.
     * @param x X value in screen coordinates
     * @param y Y value in screen coordinates
     * @return True if consumed, false if not
     */
    boolean onDoubleTap(float x, float y);

    /**
     * Event for begin scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @return True if consumed, false if not
     */
    boolean onScaleBegin(float focusX,
                         float focusY,
                         float scaleFactor,
                         float lastSpanX,
                         float lastSpanY,
                         float currentSpanX,
                         float currentSpanY);

    /**
     * Event for scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @return True if consumed, false if not
     */
    boolean onScale(float focusX,
                    float focusY,
                    float scaleFactor,
                    float lastSpanX,
                    float lastSpanY,
                    float currentSpanX,
                    float currentSpanY);

    /**
     * Event for end scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @return True if consumed, false if not
     */
    boolean onScaleEnd(float focusX,
                       float focusY,
                       float scaleFactor,
                       float lastSpanX,
                       float lastSpanY,
                       float currentSpanX,
                       float currentSpanY);

    /**
     * Handles a scroll gesture
     * @param originX origin of scroll in x
     * @param originY origin of scroll in y
     * @param distanceX Distance to handle in x
     * @param distanceY Distance to handle in y
     * @return True if consumed, false if not
     */
    boolean onScroll(float originX, float originY, float distanceX, float distanceY);

    /**
     * Handles a fling gesture
     * @param originX origin of scroll in x
     * @param originY origin of scroll in y
     * @param velocityX Fling velocity in X
     * @param velocityY Fling velocity in Y
     * @return True if consumed, false if not
     */
    boolean onFling(float originX, float originY, float velocityX, float velocityY);

    /**
     * Represents line properties
     */
    public static class LineProperties {
        public int color;
        public int alpha;
        public float width;

        /**
         * Default line property
         */
        public LineProperties() {
            color = 0xFF000000;
            width = 1;
            alpha = 0xFF;
        }

        /**
         * Accepts color and width
         * @param color Color value 0xAARRGGBB
         * @param width Width of line
         */
        public LineProperties(int color, float width) {
            this();
            this.color = color;
            this.width = width;
        }
    }

    public static class TextProperties {
        public int color;
        public int alpha;
        public boolean bold;
        public String font;
        public int size;
        public int spacing;
        public boolean centered;

        /**
         * Default line property
         */
        public TextProperties() {
            spacing = 0;
            color = 0xFFFFFFFF;
            size = 20;
            alpha = 0xFF;
            bold = false;
            centered = false;
            font = ""; //let the renderer default this
        }

        /**
         * Initialize with params
         * @param color 0xAARRGGBB format
         * @param size Size depends on renderer
         * @param font Font string, depends on renderer
         */
        public TextProperties(int color, int size, String font) {
            this();
            this.color = color;
            this.size = size;
            this.font = font;
        }
    }
}
