package com.biblo.datastreamviewer.linegraph.model;

import com.biblo.datastreamviewer.common.model.basic.Rectangle;
import com.biblo.datastreamviewer.common.model.basic.SimpleLine;
import com.biblo.datastreamviewer.common.model.basic.WritableLine;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;
import com.biblo.datastreamviewer.common.model.basic.WritableText;

import java.text.DecimalFormat;
import java.util.LinkedList;

/**
 * @author gchapman
 * Represents a modifiable grid
 */
public class WritableGrid extends Grid {

    private int[] mIndices;
    private int mGridCountGoal;
    private WritableRectangle mGridViewPort;
    private Rectangle mWorld;
    private LinkedList<WritableLine> mGridX;
    private LinkedList<WritableLine> mGridY;
    private LinkedList<WritableText> mTextX;
    private LinkedList<WritableText> mTextY;
    private Rectangle mNewViewPort;
    private DecimalFormat mTextFormatter;
    private int mCurrentIndex;
    private float mMultiplier;

    protected enum GridMode {
        X,
        Y
    }

    /**
     * Constructs a simple grid that can be modified for different
     * scale, pan
     */
    public WritableGrid(Rectangle world) {
        mWorld = world;
        mLines = new LinkedList<SimpleLine>();
        mGridX = new LinkedList<WritableLine>();
        mGridY = new LinkedList<WritableLine>();
        mTextX = new LinkedList<WritableText>();
        mTextY = new LinkedList<WritableText>();
        mGridViewPort = new WritableRectangle();
        mTextFormatter = new DecimalFormat("#.###");
        //TODO load
        mGridCountGoal = 10;
        mIndices = new int[] {1, 2, 5};
        //TODO load color, text, size
    }

    /**
     * Will set the current viewport to the grid. This causes a check for updating the grid
     * lines
     * @param viewPort View port of the display
     */
    public void setViewPort(Rectangle viewPort) {
        mNewViewPort = viewPort;
        //check scale first, if not, then panning is occurring
        boolean change = checkScale(viewPort) || checkPosition(viewPort); //uses the short circuit
        if(change) {
            mGridViewPort.set(viewPort);
        }
    }

    private boolean checkPosition(Rectangle viewPort) {
        if(!mGridViewPort.equals(viewPort)) {
            //TODO this could be more efficient than doing a full
            //grid recreation like scaling does, i.e. keep grid lines that are still in
            //view alive and only add/remove lines out of view. Leaving for now to tackle
            //higher priority items.
            updateGrid(GridMode.X, viewPort.getLeft(), viewPort.getRight(), mGridX, mTextX);
            updateGrid(GridMode.Y, viewPort.getBottom(), viewPort.getTop(), mGridY, mTextY);
            mLines.clear();
            mLines.addAll(mGridX);
            mLines.addAll(mGridY);
            mText.clear();
            mText.addAll(mTextX);
            mText.addAll(mTextY);
            return true;
        }
        return false;
    }

    private boolean checkScale(Rectangle viewPort) {
        if(!mGridViewPort.dimensionsEqual(viewPort)) { //change in scale
            updateGrid(GridMode.X, viewPort.getLeft(), viewPort.getRight(), mGridX, mTextX);
            updateGrid(GridMode.Y, viewPort.getBottom(), viewPort.getTop(), mGridY, mTextY);
            mLines.clear();
            mLines.addAll(mGridX);
            mLines.addAll(mGridY);
            mText.clear();
            mText.addAll(mTextX);
            mText.addAll(mTextY);
            return true;
        }
        return false;
    }

    private void updateGrid(GridMode mode,
                            float min,
                            float max,
                            LinkedList<WritableLine> grid,
                            LinkedList<WritableText> text) {
        //find the magnitude of view and use -1 of that for the grid line index
        float difference = max - min;
        int magnitude = getMagnitude(difference) - 1;
        mMultiplier = (float) Math.pow(10, magnitude);
        mCurrentIndex = findIndex(difference, mMultiplier);
        float value0 = findInitialValue(min, mCurrentIndex, mMultiplier);
        createGridLines(mode, value0, mCurrentIndex, mMultiplier, min, max, grid, text);
    }

    private void createGridLines(GridMode mode,
                                 float initial,
                                 int index,
                                 float multiplier,
                                 float min,
                                 float max,
                                 LinkedList<WritableLine> grid,
                                 LinkedList<WritableText> text) {
        float increment = index * multiplier;
        initial -= (increment*2); //Go to the left of initial line in view for the first
                              //line out of view just to make sure we have lines outside
                              //the view to rotate into view on pan
        max += (increment*2);     //Same for the right, go past max view
        float current = initial;
        int count = (int)((max - current) / increment);

        //size list to appropriate length
        while(grid.size() < count) { //add lines until count reached
            grid.add(new WritableLine());
        }
        while(grid.size() > count) { //remove lines until count reached
            grid.removeLast();
        }
        //size list to appropriate length
        while(text.size() < count) { //add text until count reached
            text.add(new WritableText());
        }
        while(text.size() > count) { //remove text until count reached
            text.removeLast();
        }

        for(int i = 0; i < count; ++i) {
            current += increment;
            setGrid(current, mode, grid.get(i), text.get(i));
        }
    }

    private void setGrid(float current,
                         GridMode mode,
                         WritableLine line,
                         WritableText text) {
        switch (mode) {
            case X:
                line.setStart(current, mWorld.getBottom());
                line.setEnd(current, mWorld.getTop());
                text.setText(mTextFormatter.format(current));
                text.setPoint(current, mNewViewPort.getBottom());
                break;
            case Y:
                line.setStart(mWorld.getLeft(), current);
                line.setEnd(mWorld.getRight(), current);
                text.setText(mTextFormatter.format(current));
                text.setPoint(mNewViewPort.getLeft(), current);
                break;
        }
    }

    private float findInitialValue(float min, int index, float multiplier) {
        float matchIndex = (index * multiplier);
        return (float) Math.floor(min / matchIndex) * matchIndex;
    }

    private int findIndex(float difference, float multiplier) {
        int bestIndex = 0;
        int minTarget = Integer.MAX_VALUE;
        for(int index : mIndices) {
            float base = multiplier * index;
            int estimatedCount = (int) (difference / base);
            int target = Math.abs(mGridCountGoal - estimatedCount);
            if(target < minTarget) {
                minTarget = target;
                bestIndex = index;
            }
        }
        return bestIndex;
    }

    private int getMagnitude(float value) {
        double magnitude = Math.log10(value);
        if(Double.isNaN(magnitude) || Double.isInfinite(magnitude)) {
            return 0;
        }
        return (int)Math.round(magnitude);
    }
}
