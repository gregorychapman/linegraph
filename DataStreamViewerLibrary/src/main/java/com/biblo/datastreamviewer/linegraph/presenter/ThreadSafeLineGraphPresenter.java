package com.biblo.datastreamviewer.linegraph.presenter;

import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.Rectangle;
import com.biblo.datastreamviewer.linegraph.model.Grid;
import com.biblo.datastreamviewer.linegraph.model.LineGraphModel;
import com.biblo.datastreamviewer.linegraph.model.MultiPointLine;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;
import com.biblo.datastreamviewer.linegraph.view.LineGraphView;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author gchapman
 * This provides a thread safe interaction between the model and view.
 */
public class ThreadSafeLineGraphPresenter implements LineGraphPresenter {

    private LineGraphModel mModel;
    private LineGraphView mView;
    private ReentrantLock mModelLock;
    private ReentrantLock mViewLock;
    private volatile boolean mRunning;

    /**
     * Constructs a thread safe presenter
     * @param model Model of the MVP design
     * @param view View of the MVP design
     */
    public ThreadSafeLineGraphPresenter(LineGraphModel model, LineGraphView view) {
        mView = view;
        mModel = model;
        mModelLock = new ReentrantLock();
        mViewLock = new ReentrantLock();
    }

    @Override
    public void start() {
        mView.startRender();
        mRunning = true;
    }

    @Override
    public void stop() {
        mView.stopRender(); //DO NOT LOCK VIEW! This call is already thread-safe
                            //implemented by the view, this could block on a thread that
                            //might lock the view so there is a potential deadlock if locking,
                            //therefore, don't lock
        mRunning = false;
    }

    @Override
    public LineGraphModel lockModel() {
        mModelLock.lock();
        return mModel;
    }

    @Override
    public void unlockModel() {
        mModelLock.unlock();
    }

    @Override
    public LineGraphView lockView() {
        mViewLock.lock();
        return mView;
    }

    @Override
    public void unlockView() {
        mViewLock.unlock();
    }

    @Override
    public void setGridProperties(LineProperties properties) {
        try {
            mModelLock.lock();
            Grid grid = mModel.getGrid();
            //view doesn't need to be locked, this is only modifying the line
            //object, which is part of the model.
            if (grid != null) {
                mView.addLineProperties(grid, properties.color, properties.width);
            }
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public MultiPointLine addDataLine(LineProperties properties) {
        MultiPointLine line = null;
        try {
            mModelLock.lock();
            line = mModel.addDataLine();
            //view doesn't need to be locked, this is only modifying the line
            //object, which is part of the model.
            if (line != null) {
                mView.addLineProperties(line, properties.color, properties.width);
            }
        }
        finally {
            mModelLock.unlock();
        }
        return line;
    }

    @Override
    public void removeDataLine(MultiPointLine line) {
        try {
            mModelLock.lock();
            mModel.removeDataLine(line);
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public void clear(MultiPointLine line) {
        try {
            mModelLock.lock();
            mModel.clear(line);
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public void setDataFactorX(float factorX) {
        try {
            mModelLock.lock();
            mModel.setDataFactorX(factorX);
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public void onData(MultiPointLine line, float[] data) {
        if(!mRunning) {
            return;
        }
        try {
            mModelLock.lock();
            mModel.onData(line, data);
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public void onData(MultiPointLine line, ArrayList<Float> data) {
        if(!mRunning) {
            return;
        }
        try {
            mModelLock.lock();
            mModel.onData(line, data);
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public void onText(AttributableText textObject, String text, Point location, TextProperties properties) {
        try {
            mModelLock.lock();
            mModel.onText(textObject, text, location);
            if (textObject != null) {
                mView.addTextProperties(textObject, properties.color, properties.size,
                        properties.font, properties.bold, properties.alpha,
                        properties.spacing, properties.centered);
            }
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public void getDisplayPort(WritableRectangle displayPort) {
        try {
            mViewLock.lock();
            mView.getDisplayPort(displayPort);
        }
        finally {
            mViewLock.unlock();
        }
    }

    @Override
    public void getWorldBounds(WritableRectangle worldBounds) {
        try {
            mModelLock.lock();
            worldBounds.set(mModel.getWorldBounds());
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public void setWorldBounds(Rectangle worldBounds) {
        try {
            mModelLock.lock();
            mModel.setWorldBounds(worldBounds);
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public void setViewPort(Rectangle viewPort) {
        try {
            mViewLock.lock();
            mView.setViewPort(viewPort.getLeft(),
                              viewPort.getTop(),
                              viewPort.getRight(),
                              viewPort.getBottom());
        }
        finally {
            mViewLock.unlock();
        }
    }

    @Override
    public AttributableText addText(String text, Point location, TextProperties properties) {
        AttributableText textObject = null;
        try {
            mModelLock.lock();
            textObject = mModel.addText(text, location);
            //view doesn't need to be locked, this is only modifying the text
            //object, which is part of the model.
            if (textObject != null) {
                mView.addTextProperties(textObject, properties.color, properties.size,
                                        properties.font, properties.bold, properties.alpha,
                                        properties.spacing, properties.centered);
            }
        }
        finally {
            mModelLock.unlock();
        }
        return textObject;
    }

    @Override
    public void removeText(AttributableText textObject) {
        try {
            mModelLock.lock();
            mModel.removeText(textObject);
        }
        finally {
            mModelLock.unlock();
        }
    }

    @Override
    public boolean onDown() {
        try {
            mViewLock.lock();
            return mView.onDown();
        }
        finally {
            mViewLock.unlock();
        }
    }

    @Override
    public boolean onDoubleTap(float x, float y) {
        try {
            mViewLock.lock();
            return mView.onDoubleTap(x, y);
        }
        finally {
            mViewLock.unlock();
        }
    }

    @Override
    public boolean onScaleBegin(float focusX,
                                float focusY,
                                float scaleFactor,
                                float lastSpanX,
                                float lastSpanY,
                                float currentSpanX,
                                float currentSpanY) {
        try {
            mViewLock.lock();
            return mView.onScaleBegin(focusX,
                                      focusY,
                                      scaleFactor,
                                      lastSpanX,
                                      lastSpanY,
                                      currentSpanX,
                                      currentSpanY);
        }
        finally {
            mViewLock.unlock();
        }
    }

    @Override
    public boolean onScale(float focusX,
                           float focusY,
                           float scaleFactor,
                           float lastSpanX,
                           float lastSpanY,
                           float currentSpanX,
                           float currentSpanY) {
        try {
            mViewLock.lock();
            return mView.onScale(focusX,
                                 focusY,
                                 scaleFactor,
                                 lastSpanX,
                                 lastSpanY,
                                 currentSpanX,
                                 currentSpanY);
        }
        finally {
            mViewLock.unlock();
        }
    }

    @Override
    public boolean onScaleEnd(float focusX,
                              float focusY,
                              float scaleFactor,
                              float lastSpanX,
                              float lastSpanY,
                              float currentSpanX,
                              float currentSpanY) {
        try {
            mViewLock.lock();
            return mView.onScaleEnd(focusX,
                                    focusY,
                                    scaleFactor,
                                    lastSpanX,
                                    lastSpanY,
                                    currentSpanX,
                                    currentSpanY);
        }
        finally {
            mViewLock.unlock();
        }
    }

    @Override
    public boolean onScroll(float originX, float originY, float distanceX, float distanceY) {
        try {
            mViewLock.lock();
            return mView.onScroll(originX, originY, distanceX, distanceY);
        }
        finally {
            mViewLock.unlock();
        }
    }

    @Override
    public boolean onFling(float originX, float originY, float velocityX, float velocityY) {
        try {
            mViewLock.lock();
            return mView.onFling(originX, originY, velocityX, velocityY);
        }
        finally {
            mViewLock.unlock();
        }
    }

}
