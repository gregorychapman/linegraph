package com.biblo.datastreamviewer.linegraph.model;

import com.biblo.datastreamviewer.common.model.basic.WritablePoint;

import java.util.ArrayList;

/**
 * @author gchapman
 * Represents a line that can have its points modified after creation
 */
public class WritableDataLine extends MultiPointLine {

    /**
     * Updates the line with the single dimension data. Uses values for Y and
     * index into array as X.
     * @param singleDimension Single dimension data array
     */
    public void onData(float[] singleDimension) {
        onData(singleDimension, 1);
    }

    /**
     * Updates the line with the single dimension data. Uses values for Y and
     * index into array as X.
     * @param singleDimension Single dimension data array
     * @param dataFactorX Multiplier for x values
     */
    public void onData(float[] singleDimension, float dataFactorX) {
        float y;
        checkBufferSize(singleDimension.length);
        for(int x = 0; x < singleDimension.length; ++x) {
            y = singleDimension[x];
            WritablePoint point = (WritablePoint) mPoints.get(x); //we know this should always be
                                                                  //writable in this class
            point.setX(x*dataFactorX);
            point.setY(y);
        }
    }

    /**
     * Updates the line with the single dimension data. Uses values for Y and
     * index into array as X.
     * @param singleDimension Single dimension data array
     */
    public void onData(ArrayList<Float> singleDimension) {
        onData(singleDimension, 1);
    }

    /**
     * Updates the line with the single dimension data. Uses values for Y and
     * index into array as X.
     * @param singleDimension Single dimension data array
     * @param dataFactorX Multiplier for x values
     */
    public void onData(ArrayList<Float> singleDimension, float dataFactorX) {
        float y;
        int count = singleDimension.size();
        checkBufferSize(count);
        for(int x = 0; x < count; ++x) {
            y = singleDimension.get(x);
            WritablePoint point = (WritablePoint) mPoints.get(x); //we know this should always be
            //writable in this class
            point.setX(x*dataFactorX);
            point.setY(y);
        }
    }

    private void checkBufferSize(int size) {
        if(mPoints.size() < size) {
            int difference = size - mPoints.size();
            for(int i = 0; i < difference; ++i) {
                mPoints.add(new WritablePoint());
            }
        }
        else if(mPoints.size() > size) {
            int difference = size - mPoints.size();
            int removeIndex = mPoints.size() - 1;
            for(int i = 0; i < difference; ++i, --removeIndex) {
                mPoints.remove(removeIndex);
            }
        }
    }

    public void clear() {
        mPoints.clear();
    }
}
