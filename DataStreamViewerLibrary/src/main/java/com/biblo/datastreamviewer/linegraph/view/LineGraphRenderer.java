package com.biblo.datastreamviewer.linegraph.view;

import com.biblo.datastreamviewer.common.model.basic.*;
import com.biblo.datastreamviewer.linegraph.model.Grid;
import com.biblo.datastreamviewer.linegraph.model.LineGraphModel;
import com.biblo.datastreamviewer.linegraph.model.MultiPointLine;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;

import java.util.ArrayList;

/**
 * @author gchapman
 * Abstract class that will act as a template pattern for rendering the line graph
 * scene. This object is used mainly in the renderloop thread only. Make sure any
 * items from other threads are thread-safe in derivatives
 */
public abstract class LineGraphRenderer extends PropertyObject {

    private static final int FRAME_RATE_CALCULATION_EVENT_COUNT = 10;

    private Thread mRenderLoopThread;
    private RenderLoop mRenderLoop;
    protected WritablePoint mScreenResolution;
    protected float mFrameRate;
    protected WritableText mFrameRateText;
    protected WritableRectangle mViewPort; //world coordinates
    protected WritableRectangle mDisplayPort; //pixel coordinates
    protected WritableRectangle mWorldBounds; //world coordinates
    protected boolean mInit;
    protected volatile boolean mRunning = false;

    protected PathLine mLinePath;
    private PointDecimation mDecimator;
    private EnvelopeDecimation mEnvelopeDecimator;
    private BoundaryDecimation mBoundaryDecimator;
    private float mDecimationEventFactor; //Ratio 0..1, lower means less quality for data

    /**
     * Constructs the renderer with presenter.
     * @param presenter Presenter of MVP design
     */
    public LineGraphRenderer(LineGraphPresenter presenter) {
        mRenderLoop = new RenderLoop(presenter, this);
        mViewPort = new WritableRectangle(); //world coordinates
        mDisplayPort = new WritableRectangle(); //pixel coordinates
        mWorldBounds = new WritableRectangle(); //world coordinates
        mScreenResolution = new WritablePoint();
        mFrameRateText = new WritableText();
        mEnvelopeDecimator = new EnvelopeDecimation();
        mDecimator = mBoundaryDecimator = new BoundaryDecimation();
        mDecimationEventFactor = 0.50f;
    }

    /**
     * Must call this for the renderer to work.
     */
    public void init() {
        mInit = true;
        mLinePath = createLine();
    }

    /**
     * Starts the render loop
     */
    public void start() {
        if(!mInit) {
            return;
        }
        mRunning = true;
        mRenderLoopThread = new Thread(mRenderLoop);
        mRenderLoopThread.start();
    }

    /**
     * Stops render loop and waits for it to complete
     */
    public void stop() {
        mRunning = false;
        if(mRenderLoopThread == null) {
            return;
        }

        mRenderLoopThread.interrupt();
        try {
            mRenderLoopThread.join();
        } catch (InterruptedException e) {
        }
        mRenderLoopThread = null;
    }

    /**
     * @return Scale of the display to view
     */
    protected float getScaleX() {
        return (mDisplayPort.getWidth() / mViewPort.getWidth());
    }

    /**
     * @return Scale of the display to view
     */
    protected float getScaleY() {
        return (mDisplayPort.getHeight() / mViewPort.getHeight());
    }

    /**
     * Gets the screen coordinates for the view point
     * @param viewX Value to convert
     * @param scaleX Scale of view to display
     * @return Converted value
     */
    protected float getScreenX(float viewX, float scaleX) {
        return ((viewX - mViewPort.getLeft()) * scaleX) - mDisplayPort.getLeft();
    }

    /**
     * Gets the screen coordinates for the view point
     * @param viewY Value to convert
     * @param scaleY Scale of view to display
     * @return Converted value
     */
    protected float getScreenY(float viewY, float scaleY) {
        return mDisplayPort.getTop() - ((viewY - mViewPort.getBottom()) * scaleY);
    }

    /**
     * Will set the current calculated frame rate of the renderer
     * @param frameRate Frame rate calculated
     */
    public void setFrameRate(float frameRate) {
        mFrameRate = frameRate;
        mFrameRateText.setText((Integer.toString((int)mFrameRate)));
    }

    /**
     * Will render the model
     * @param model Model to render with
     */
    public void render(LineGraphModel model) {
        drawGrid(model);
        drawData(model);
        drawText(model);
        drawDebug();
    }

    protected void drawText(LineGraphModel model) {
        for(AttributableText text : model.getTextObjects()) {
            drawScreenText(text.getText(), text);
        }
    }

    private void drawGrid(LineGraphModel model) {
        Grid grid = model.getGrid();
        drawGridLines(grid);
        drawGridText(grid);
    }

    private void drawGridLines(Grid grid) {
        for(SimpleLine line : grid.getLines()) {
            drawLine(line, grid);
        }
    }

    private void drawGridText(Grid grid) {
        for(SimpleText text : grid.getText()) {
            drawText(text, grid);
        }
    }

    private void drawData(LineGraphModel model) {
        MultiPointLine[] lines = model.getDataLines();
        for(MultiPointLine line : lines) {
            drawLine(line);
        }
    }

    protected void drawDebug() {
        mFrameRateText.setPoint(mDisplayPort.getRight() - 50, mDisplayPort.getTop() - 150);
        drawScreenText(mFrameRateText, this);
    }

    /**
     * Will draw a multi point line. Prefer implementation to use a Path, if available
     * @param line Multiple points representing contiguous line
     */
    protected void drawLine(MultiPointLine line) {
        final ArrayList<Point> points = line.getPoints();
        int pointLimit = (int) (mScreenResolution.getX() * mDecimationEventFactor);
        float scaleX = getScaleX();
        float scaleY = getScaleY();
        mDecimator = mBoundaryDecimator;
        int drawCount = drawLineDecimate(1, points, scaleX, scaleY, pointLimit, mLinePath);
        if(drawCount > 0) { //if gt 0, then we need to decimate
            int decimate = (int) Math.ceil(drawCount / (double) pointLimit); //find an appropriate decimation
            mDecimator = mEnvelopeDecimator;
            drawLineDecimate(decimate, points, scaleX, scaleY, 0, mLinePath);
        }
        mLinePath.draw();
    }

    /**
     * Will draw the line with the set decimator.
     * @param decimate Decimation factor
     * @param points Data line
     * @param scaleX X scale display to view
     * @param scaleY Y scale display to view
     * @param maxCount Max count of points, if limit reached, it will abort.
     *                 Set <= 0 for no limit.
     * @return if < 0, then the draw succeeded, if it is > 0 then decimation is needed and
     *         this is the count of points in view
     */
    private int drawLineDecimate(int decimate,
                                 ArrayList<Point> points,
                                 float scaleX,
                                 float scaleY,
                                 int maxCount,
                                 PathLine path) {
        if (mDecimator == null) {
            return 0;
        }

        int count = 1;
        Point point;
        mDecimator.setDecimationFactor(decimate);
        mDecimator.setBounds(mViewPort);
        mDecimator.initialize(points);
        path.reset();
        while (mDecimator.hasNext()) {
            point = mDecimator.next();
            if (maxCount <= 0 || count < maxCount) {
                path.add(getScreenX(point.getX(), scaleX),
                        getScreenY(point.getY(), scaleY));
            }
            ++count;
        }
        return count < maxCount ? 0 : count;
    }

    protected void drawLine(SimpleLine line, PropertyObject owner) {
        //Since this is simple, to avoid confusion, the lines are manually scaled, clipped

        //Manually scale, translate
        float scaleX = getScaleX();
        float scaleY = getScaleY();
        float startX = getScreenX(line.getStart().getX(), scaleX);
        float startY = getScreenY(line.getStart().getY(), scaleY);
        float endX = getScreenX(line.getEnd().getX(), scaleX);
        float endY = getScreenY(line.getEnd().getY(), scaleY);

        //Manually clip

        //check if start and end are out of view, if so, don't bother
        //This leans on the side of drawing diagonal lines that might not be in
        //view but still pass this check. This could be more efficient.
        if((startX < mDisplayPort.getLeft() && endX < mDisplayPort.getLeft()) ||
                (startX > mDisplayPort.getRight() && endX > mDisplayPort.getRight())) {
            return;
        }
        if((startY < mDisplayPort.getBottom() && endY < mDisplayPort.getBottom()) ||
                (startY > mDisplayPort.getTop() && endY > mDisplayPort.getTop())) {
            return;
        }

        //note: display port is bottom up
        startX = Math.max(startX, mDisplayPort.getLeft());
        startX = Math.min(startX, mDisplayPort.getRight());
        startY = Math.max(startY, mDisplayPort.getBottom());
        startY = Math.min(startY, mDisplayPort.getTop());
        endX = Math.max(endX, mDisplayPort.getLeft());
        endX = Math.min(endX, mDisplayPort.getRight());
        endY = Math.max(endY, mDisplayPort.getBottom());
        endY = Math.min(endY, mDisplayPort.getTop());

        drawLine(startX, startY, endX, endY, owner);
    }

    /**
     * This class is the render loop, it will continue to render in an
     * endless loop until it is interrupted.
     */
    private static class RenderLoop implements Runnable
    {
        private final LineGraphPresenter mLineGraphPresenter;
        private final LineGraphRenderer mRenderer;
        private long mLastTime;
        private int mFrameCount;
        private long mFrameCountStartTime;

        public RenderLoop(LineGraphPresenter presenter, LineGraphRenderer renderer) {
            mLineGraphPresenter = presenter;
            mRenderer = renderer;
        }

        @Override
        public void run() {
            if(mLineGraphPresenter == null || mRenderer == null) {
                return;
            }
            if(!mRenderer.initRender()) {
                return; //if we fail init, exit
            }
            mLastTime = mRenderer.getTimeNow();
            while(mRenderer.mRunning && !Thread.currentThread().isInterrupted()) {
                try {
                    if(mRenderer.beginRender()) {

                        long timeNow = mRenderer.getTimeNow();
                        long deltaTime = timeNow - mLastTime;
                        mLastTime = timeNow;

                        //NOTE: we should avoid deadlock issues by not having nested locks
                        //in the renderer for the view and model, they are separate blocks

                        mLineGraphPresenter.getWorldBounds(mRenderer.mWorldBounds);

                        //grab updates from the view which could affect:
                        // viewport
                        try {
                            LineGraphView view = mLineGraphPresenter.lockView();
                            view.setWorldBounds(mRenderer.mWorldBounds);
                            mRenderer.mScreenResolution.set(view.getScreenResolution());
                            view.update(deltaTime);
                            view.getViewPort(mRenderer.mViewPort);
                            view.getDisplayPort(mRenderer.mDisplayPort);
                        } catch(Exception exception) {
                            mRenderer.error(exception.getMessage());
                        } finally {
                            mLineGraphPresenter.unlockView();
                        }

                        try {
                            LineGraphModel model = mLineGraphPresenter.lockModel();
                            model.setViewPort(mRenderer.mViewPort);
                            model.update(deltaTime);
                            mRenderer.render(model);
                            calculateFrameRate();
                        } catch(Exception exception) {
                            mRenderer.error(exception.getMessage());
                        } finally {
                            mLineGraphPresenter.unlockModel();
                        }
                    }

                } catch(Exception exception) {
                    mRenderer.error(exception.getMessage());
                } finally {
                    mRenderer.endRender();
                }
            }
            mRenderer.finishRender();
        }

        private void calculateFrameRate() {
            if (mFrameCount-- <= 0) {
                mFrameCount = FRAME_RATE_CALCULATION_EVENT_COUNT;
                float frameCountTimeDelta = (mRenderer.getTimeNow() - mFrameCountStartTime) / 1000f;
                mRenderer.setFrameRate(FRAME_RATE_CALCULATION_EVENT_COUNT / frameCountTimeDelta);
                mFrameCountStartTime = mRenderer.getTimeNow();
            }
        }
    }

    /**
     * @return Create an implementation of the data line
     */
    protected abstract PathLine createLine();

    /**
     * Will add platform specific line properties to associate with the object
     * for future rendering.
     * @param object Property object to associate properties with
     * @param color Color of the line
     * @param width Width of the line
     */
    public abstract void addLineProperties(PropertyObject object, int color, float width);

    /**
     * Adds renderer properties using the intermediate parameters.
     * @param textObject Text object to associate properties with
     * @param color Color of text
     * @param size Size of text
     * @param font Font to use
     * @param bold Bold if true
     * @param alpha Alpha blending
     * @param spacing Spacing of text from location
     * @param centered Centered text if true, left otherwise
     */
    public abstract void addTextProperties(AttributableText textObject, int color, int size,
                                           String font, boolean bold, int alpha, int spacing,
                                           boolean centered);

    /**
     * Draws a simple line.
     * @param startX Start x point
     * @param startY Start y point
     * @param endX End x point
     * @param endY End y point
     * @param owner Property tags
     */
    protected abstract void drawLine(float startX, float startY, float endX, float endY, PropertyObject owner);

    /**
     * Will draw text associated with a property object
     * @param text Text to draw
     * @param owner Property tags
     */
    protected abstract void drawText(SimpleText text, PropertyObject owner);

    /**
     * Will draw text associated with a property object, in screen coordinates
     * @param text Text to draw, with screen coordinates
     * @param owner Property tags
     */
    protected abstract void drawScreenText(SimpleText text, PropertyObject owner);

    /**
     * Called just before render loop to setup on render thread
     * @return True if rendering can continue
     */
    protected abstract boolean initRender();

    /**
     * Begin the rendering process, clean slate
     * @return True if rendering can continue
     */
    protected abstract boolean beginRender();

    /**
     * End the render, submit render output to view (if double buffering)
     */
    protected abstract void endRender();

    /**
     * Called at the end of the render loop quitting, on render thread
     */
    protected abstract void finishRender();

    /**
     * @return Returns platform specific time stamp, ms
     */
    protected abstract long getTimeNow();

    /**
     * Handles platform specific error messaging
     * @param message Message for error to give context
     */
    protected abstract void error(String message);
}
