package com.biblo.datastreamviewer.linegraph.view;

import com.biblo.datastreamviewer.common.model.basic.Point;

import java.util.ArrayList;

/**
 * @author gchapman
 * Doesn't decimate, only pass-through and bounds.
 */
public class BoundaryDecimation extends PointDecimation {

    protected boolean mStartFound;
    protected float mEndCheck;
    protected int mIndex;
    protected boolean mEndFound;
    protected int mCount;

    @Override
    public void setDecimationFactor(int decimation) {
        super.setDecimationFactor(1); //pass through only
    }

    @Override
    protected void initialize() {
        mStartFound = false;
        mEndCheck = mBoundary.getRight();
        mIndex = 0;
        mEndFound = false;
        mCount = mData.size();
    }

    @Override
    protected boolean findNext() {
        boolean complete = mEndFound; //delay completion when end is found to include last point
        findStart();
        if(!mEndFound) {
            mEndFound = findNext(mData, mIndex, mEndCheck);
            ++mIndex;
            mEndFound = mEndFound || mIndex >= mCount;
        }
        return !complete;
    }

    protected boolean findNext(final ArrayList<Point> points,
                             int index,
                             float check) {
        float x = 0;
        if(index < mCount) {
            x = points.get(index).getX();
            mNextPoint.setX(x);
            mNextPoint.setY(points.get(index).getY());
        }
        return x >= check;
    }

    protected void findStart() {
        if(!mStartFound) {
            float check = mBoundary.getLeft();
            int count = mData.size();
            for(mIndex = 0; mIndex < count; ++mIndex) {
                if(check < mData.get(mIndex).getX()) {
                    break; //Found start
                }
            }
            --mIndex;
            if(mIndex < 0) {
                mIndex = 0;
            }
            mStartFound = true;
        }
    }
}
