package com.biblo.datastreamviewer.linegraph.model;

import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.PropertyObject;

import java.util.ArrayList;

/**
 * @author gchapman
 * Represents a data line with properties
 * These lines are assumed to be monotonic.
 * @see <a href="http://en.wikipedia.org/wiki/Monotonic_function">Monotonic_function</a>
 */
public class MultiPointLine extends PropertyObject {

    protected ArrayList<Point> mPoints;

    /**
     * Default holds a list of points
     */
    public MultiPointLine() {
        mPoints = new ArrayList<Point>();
    }

    /**
     * @return Multi point line, list of points
     */
    public final ArrayList<Point> getPoints() {
        return mPoints;
    }
}
