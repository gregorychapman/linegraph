package com.biblo.datastreamviewer.linegraph.view;

import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.PropertyObject;
import com.biblo.datastreamviewer.common.model.basic.Rectangle;
import com.biblo.datastreamviewer.common.model.basic.WritablePoint;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;

/**
 * @author gchapman
 * Interface for the line graph view MVP
 */
public interface LineGraphView {

    /**
     * Will start the render loop. Thread-safe
     */
    void startRender();

    /**
     * Will stop the render loop. Thread-safe
     */
    void stopRender();

    /**
     * Must be called with a valid presenter or the program will crash
     * @param presenter Presenter of the MVP framework
     */
    void setPresenter(LineGraphPresenter presenter);

    /**
     * This will add specific attributes created view specific for the object which
     * holds the properties. There shall be no modification of the view.
     * @param object Object to modify
     * @param color Color of the line
     * @param width Width of the line
     */
    void addLineProperties(PropertyObject object, int color, float width);

    /**
     * Adds renderer properties using the intermediate parameters.
     * @param textObject Text object to associate properties with
     * @param color Color of text
     * @param size Size of text
     * @param font Font to use
     * @param bold Bold if true
     * @param alpha Alpha blending
     * @param spacing Spacing from text position
     * @param centered Center text if true, left otherwise
     */
    void addTextProperties(AttributableText textObject, int color, int size, String font,
                           boolean bold, int alpha, int spacing, boolean centered);

    /**
     * Periodic update for animation
     * @param deltaTimeMillis Time passed since last update
     */
    void update(long deltaTimeMillis);

    /**
     * Will return the view port which is in domain specific units
     * @param viewPort World coordinates of the view. Write the view values
     *                 to this object
     * @return Rectangle representing the view port
     */
    Rectangle getViewPort(WritableRectangle viewPort);

    /**
     * Will return the view port which is in pixel units
     * @param displayPort Pixel coordinates of the view, bottom-up. Write
     *                    the view values to this object
     * @return Rectangle representing the display port
     */
    Rectangle getDisplayPort(WritableRectangle displayPort);

    /**
     * Will set the world bounds to the view
     * @param worldBounds Rectangle in world coordinates representing
     *                    bounds of world
     */
    void setWorldBounds(Rectangle worldBounds);

    /**
     * Will set the view port to the specified values in world coordinate
     * @param left Left edge of view
     * @param top Top edge of view
     * @param right Right edge of view
     * @param bottom Bottom edge of view
     */
    void setViewPort(float left, float top, float right, float bottom);

    /**
     * Will set the view port to the specified values in world coordinate
     * @param left Left edge of view
     * @param bottom Bottom edge of view
     */
    void setViewPort(float left, float bottom);

    /**
     * Convert pixel x,y to world coordinates
     * @param px pixel x
     * @param py pixel y
     * @param viewPoint Converted point to world coordinates or null
     * @return True if the point was in the display port
     */
    boolean displayToView(float px, float py, WritablePoint viewPoint);

    /**
     * Convert view x,y to display coordinates
     * @param x view x
     * @param y view y
     * @param displayPoint Converted point to display coordinates or null
     * @return True if the point was in the display port
     */
    boolean viewToDisplay(float x, float y, WritablePoint displayPoint);

    /**
     * Convert pixel spans x,y to world coordinates
     * @param originX origin of scroll in x
     * @param originY origin of scroll in y
     * @param spanX pixel span x
     * @param span Converted point to world coordinates or null
     */
    boolean displayToViewDistance(float originX, float originY, float spanX, float spanY, WritablePoint span);

    /**
     * Convert view x,y to display coordinates
     * @param originX view x
     * @param originY view y
     * @param spanX Length in x
     * @param spanY Length in y
     * @param span Converted point to display coordinates or null
     * @return True if the point was in the display port
     */
    boolean viewToDisplayDistance(float originX, float originY, float spanX, float spanY, WritablePoint span);

    /**
     * @return Point with x, y resolution in pixels
     */
    Point getScreenResolution();

    /**
     * Handles a user input click down
     * @return True if handled
     */
    boolean onDown();

    /**
     * Occurs when user double taps a location
     * @param x Pixel x
     * @param y Pixel y
     * @return True if handled
     */
    boolean onDoubleTap(float x, float y);

    /**
     * Event for begin scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @return True if consumed, false if not
     */
    boolean onScaleBegin(float focusX,
                         float focusY,
                         float scaleFactor,
                         float lastSpanX,
                         float lastSpanY,
                         float currentSpanX,
                         float currentSpanY);

    /**
     * Event for scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @return True if consumed, false if not
     */
    boolean onScale(float focusX,
                    float focusY,
                    float scaleFactor,
                    float lastSpanX,
                    float lastSpanY,
                    float currentSpanX,
                    float currentSpanY);

    /**
     * Event for end scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @return True if consumed, false if not
     */
    boolean onScaleEnd(float focusX,
                       float focusY,
                       float scaleFactor,
                       float lastSpanX,
                       float lastSpanY,
                       float currentSpanX,
                       float currentSpanY);

    /**
     * Handles a scroll gesture
     * @param originX origin of scroll in x
     * @param originY origin of scroll in y
     * @param distanceX Distance to handle in x
     * @param distanceY Distance to handle in y
     * @return True if consumed, false if not
     */
    boolean onScroll(float originX, float originY, float distanceX, float distanceY);

    /**
     * Handles a fling gesture
     * @param originX origin of scroll in x
     * @param originY origin of scroll in y
     * @param velocityX Fling velocity in X
     * @param velocityY Fling velocity in Y
     * @return True if consumed, false if not
     */
    boolean onFling(float originX, float originY, float velocityX, float velocityY);
}
