package com.biblo.datastreamviewer.linegraph.model;

import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.Rectangle;

import java.util.ArrayList;

/**
 * @author gchapman
 * Interface for the line graph model MVP
 */
public interface LineGraphModel {

    /**
     * Will update the model with time duration, use for animation
     * @param deltaTimeMillis Delta time since last update
     */
    void update(long deltaTimeMillis);

    /**
     * @return Data lines to draw
     */
    MultiPointLine[] getDataLines();

    /**
     * Adds a data line to the model
     * @return Line that was added, keep this for modification of data with presenter
     *         @see com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter
     */
    MultiPointLine addDataLine();

    /**
     * Removes the line from the model
     * @param line Line to remove
     */
    void removeDataLine(MultiPointLine line);

    /**
     * Clears the line of data
     * @param line Line to clear
     */
    void clear(MultiPointLine line);

    /**
     * Will update the line with the specified data.
     * This is a single dimension data which means the float values are the y
     * values and the x values are the index. Use the presenter to update this.
     * @see com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter
     * @param line Line to update
     * @param data Data to update the line with
     */
    void onData(MultiPointLine line, float[] data);

    /**
     * Will update the line with the specified data.
     * This is a single dimension data which means the float values are the y
     * values and the x values are the index. Use the presenter to update this.
     * @see com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter
     * @param line Line to update
     * @param data Data to update the line with
     */
    void onData(MultiPointLine line, ArrayList<Float> data);

    /**
     * Will set the world boundary which is the max,min the world can scroll to.
     * @param worldBounds World bounds
     */
    void setWorldBounds(Rectangle worldBounds);

    /**
     * @return World bounds
     */
    Rectangle getWorldBounds();

    /**
     * Sets the view port from the view, this can be referenced in update() for animation.
     * The return value is the modified view port, this is the final viewport and should be
     * cascaded to other objects.
     * @param viewPort View port of the view (what the display can see in world coordinates).
     *
     */
    void setViewPort(Rectangle viewPort);

    /**
     * @return The grid that gives context to the data, represented in the model.
     */
    Grid getGrid();

    /**
     * Will set the factor for x values. Default is identity (1).
     * @param factorX Scale x values by factorX
     */
    void setDataFactorX(float factorX);

    /**
     * Adds a text object
     * @param text Text to appear in the display
     * @param location Screen location (bottom-up y)
     */
    AttributableText addText(String text, Point location);

    /**
     * Will remove the text object from the model
     * @param textObject Text to remove
     */
    void removeText(AttributableText textObject);

    /**
     * @return Text objects in the model
     */
    AttributableText[] getTextObjects();

    /**
     * Call when text needs updates
     * @param textObject Text object to update
     * @param text Text to set
     * @param location Location of text
     */
    void onText(AttributableText textObject, String text, Point location);
}
