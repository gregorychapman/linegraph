package com.biblo.datastreamviewer.linegraph.view;

import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.WritablePoint;

import java.util.ArrayList;

/**
 * @author gchapman
 * Decimates points with a strategy similar to an envelope detector.
 * The strategy is to find a min, max from a given window of the data and to use the 2 points as the
 * decimated values.
 */
public class EnvelopeDecimation extends PointDecimation {

    protected int mEnvelopeDecimation;
    protected boolean mStartFound;
    protected float mEndCheck;
    protected int mIndex;
    protected WritablePoint[] mEnvelopeDecimationBuffer;
    protected int mCurrentPoint;
    protected boolean mEndFound;
    protected int mCount;

    /**
     * Will set up a decimation for points
     */
    public EnvelopeDecimation() {
        mEnvelopeDecimationBuffer = new WritablePoint[2];
        mEnvelopeDecimationBuffer[0] = new WritablePoint();
        mEnvelopeDecimationBuffer[1] = new WritablePoint();
    }

    @Override
    public void setDecimationFactor(int decimation) {
        super.setDecimationFactor(decimation);
        //envelope will use 2 values for one decimation
        mEnvelopeDecimation = mDecimation*2;
    }

    @Override
    protected void initialize() {
        mStartFound = false;
        mEndCheck = mBoundary.getRight();
        mIndex = 0;
        mCurrentPoint = 0;
        mEndFound = false;
        mCount = mData.size();
    }

    @Override
    protected boolean findNext() {
        findStart();
        if(mCurrentPoint == 0 && !mEndFound) {
            mEndFound = findPointWindowEnvelope(mData, mIndex, mEnvelopeDecimation, mEndCheck);
            mIndex += mEnvelopeDecimation;
            mEndFound = mEndFound || mIndex >= mCount;
        }
        mNextPoint.set(mEnvelopeDecimationBuffer[mCurrentPoint]);
        mCurrentPoint = (mCurrentPoint + 1) & 0x1; //0 or 1
        return !(mEndFound && mCurrentPoint == 0); //Stop when 0,1 of the end are given
    }

    private void findStart() {
        if(!mStartFound) {
            float check = mBoundary.getLeft();
            int count = mData.size();
            for(mIndex = 0; mIndex < count; mIndex+=mEnvelopeDecimation) {
                if(check < mData.get(mIndex).getX()) {
                    break; //Found start
                }
            }
            mIndex -= mEnvelopeDecimation;
            if(mIndex < 0) {
                mIndex = 0;
            }
            mStartFound = true;
        }
    }

    private boolean findPointWindowEnvelope(final ArrayList<Point> points,
                                            int start,
                                            int decimation,
                                            float check) {
        float maxY = -Float.MAX_VALUE;
        float minY = Float.MAX_VALUE;
        int minIndex = -1;
        int maxIndex = -1;
        float x = 0;
        float y = 0;
        for(int i = start, j = 0; j < decimation && i < mCount; ++i, ++j) {
            y = points.get(i).getY();
            if(y <= minY) {
                minY = y;
                minIndex = i;
            }
            if(y > maxY) {
                maxY = y;
                maxIndex = i;
            }
        }
        if(minIndex != -1 && maxIndex != -1) {
            if (minIndex < maxIndex) {
                x = points.get(maxIndex).getX();
                mEnvelopeDecimationBuffer[0].setX(points.get(minIndex).getX());
                mEnvelopeDecimationBuffer[0].setY(minY);
                mEnvelopeDecimationBuffer[1].setX(x);
                mEnvelopeDecimationBuffer[1].setY(maxY);
            } else {
                x = points.get(minIndex).getX();
                mEnvelopeDecimationBuffer[0].setX(points.get(maxIndex).getX());
                mEnvelopeDecimationBuffer[0].setY(maxY);
                mEnvelopeDecimationBuffer[1].setX(x);
                mEnvelopeDecimationBuffer[1].setY(minY);
            }
        }
        return x >= check;
    }
}
