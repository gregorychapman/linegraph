package com.biblo.datastreamviewer.linegraph.view;

import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.Rectangle;
import com.biblo.datastreamviewer.common.model.basic.WritablePoint;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author gchapman
 * Decimates points based on a particular strategy. This allows better efficiency for
 * rendering when the number of points is greater than the user can see. Cannot remove from this
 * Iterator.
 */
public abstract class PointDecimation implements Iterator<Point> {

    protected WritablePoint mNextPoint;
    private WritablePoint mPoint;
    protected int mDecimation;
    protected Rectangle mBoundary;
    protected ArrayList<Point> mData;
    protected boolean mIsNextValid;

    /**
     * Will set up a decimation for points
     */
    public PointDecimation() {
        mPoint = new WritablePoint();
        mNextPoint = new WritablePoint();
        mIsNextValid = false;
    }

    /**
     * Will set the decimation factor
     * @param decimation Factor to decimate by
     */
    public void setDecimationFactor(int decimation) {
        mDecimation = decimation;
    }

    /**
     * Will set the bounds to help decimation
     * @param bounds Bounds
     */
    public void setBounds(Rectangle bounds) {
        mBoundary = bounds;
    }

    /**
     * Initializes the iterator with points data set.
     * NOTE: bounds and decimation factor must be set before calling this.
     * @param points Data set to decimate
     */
    public void initialize(ArrayList<Point> points) {
        mData = points;
        mIsNextValid = false;
        //boundary must be set
        if(mBoundary == null || mDecimation <= 0 || points.size() == 0){
            return;
        }

        initialize();
        next();
    }

    @Override
    public boolean hasNext() {
        return mIsNextValid;
    }

    @Override
    public Point next() {
        WritablePoint swap = mPoint;
        mPoint = mNextPoint;
        mNextPoint = swap;
        mIsNextValid = findNext();
        return mPoint;
    }

    /**
     * This is a read-only iterator
     */
    @Override
    public void remove() {
        //Does nothing
    }

    /**
     * Must modify mNextPoint
     * @return True if next is valid, false if not
     */
    protected abstract boolean findNext();

    /**
     * Initializes the derivative
     */
    protected abstract void initialize();
}
