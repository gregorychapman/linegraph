package com.biblo.datastreamviewer.linegraph.model;

import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.AttributableWritableText;
import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.Rectangle;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author gchapman
 * This model provides a scalable line graph display; able to pan and zoom, this gives the user
 * much more flexibily to view their data
 */
public class SimpleLineGraphModel implements LineGraphModel {

    protected WritableRectangle mViewPort; //used for grid lines
    protected WritableRectangle mWorldBounds;
    protected MultiPointLine[] mDataLines;
    protected AttributableText[] mText;
    protected WritableGrid mGrid;
    protected float mDataFactorX;

    /**
     * Creates a simple model: lines, grid, world
     */
    public SimpleLineGraphModel() {
        mDataLines = new MultiPointLine[0];
        mText = new AttributableText[0];
        //TODO load/save from outside model?
        mWorldBounds = new WritableRectangle(-100, -100, 20000, 2000);
        mViewPort = new WritableRectangle();
        mGrid = new WritableGrid(mWorldBounds);
        mDataFactorX = 1;
    }

    @Override
    public void update(long deltaTimeMillis) {
        //nothing for now
    }

    @Override
    public MultiPointLine[] getDataLines() {
        //this occurs very frequently, so we don't want this to be an expensive
        //call, therefore, we are maintaining an array rather than a list
        return mDataLines;
    }

    @Override
    public MultiPointLine addDataLine() {
        //This is inefficient to add new lines, however, this should happen very infrequently
        //and we need to maintain an array because it will be requested often
        MultiPointLine newLine = new WritableDataLine();
        int newIndex = mDataLines.length;
        mDataLines = Arrays.copyOf(mDataLines, newIndex + 1);
        mDataLines[newIndex] = newLine;
        return newLine;
    }

    @Override
    public void removeDataLine(MultiPointLine line) {
        List<MultiPointLine> helper = Arrays.asList(mDataLines);
        helper.remove(line);
        mDataLines = (MultiPointLine[]) helper.toArray();
    }

    @Override
    public void clear(MultiPointLine line) {
        WritableDataLine writeLine = (WritableDataLine) line;
        writeLine.clear();
    }

    @Override
    public void onData(MultiPointLine line, float[] data) {
        WritableDataLine writeLine = (WritableDataLine) line;
        writeLine.onData(data, mDataFactorX);
    }

    @Override
    public void onData(MultiPointLine line, ArrayList<Float> data) {
        WritableDataLine writeLine = (WritableDataLine) line;
        writeLine.onData(data, mDataFactorX);
    }

    @Override
    public void setWorldBounds(Rectangle worldBounds) {
        mWorldBounds.set(worldBounds);
    }

    @Override
    public Rectangle getWorldBounds() {
        return mWorldBounds;
    }

    @Override
    public void setViewPort(Rectangle viewPort) {
        mViewPort.set(viewPort);
        mGrid.setViewPort(viewPort);
    }

    @Override
    public Grid getGrid() {
        return mGrid;
    }

    @Override
    public void setDataFactorX(float factorX) {
        mDataFactorX = factorX;
    }

    @Override
    public AttributableText addText(String text, Point location) {
        //This is inefficient to add new lines, however, this should happen very infrequently
        //and we need to maintain an array because it will be requested often
        AttributableText newText = new AttributableWritableText(text, location);
        int newIndex = mText.length;
        mText = Arrays.copyOf(mText, newIndex + 1);
        mText[newIndex] = newText;
        return newText;
    }

    @Override
    public void removeText(AttributableText textObject) {
        List<AttributableText> helper = Arrays.asList(mText);
        helper.remove(textObject);
        mText = (AttributableText[]) helper.toArray();
    }

    @Override
    public AttributableText[] getTextObjects() {
        return mText;
    }

    @Override
    public void onText(AttributableText textObject, String text, Point location) {
        AttributableWritableText writeText = (AttributableWritableText) textObject;
        writeText.setPoint(location);
        writeText.setText(text);
    }
}
