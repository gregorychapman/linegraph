package com.biblo.datastreamviewer.linegraph.view.lwjgl;

import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.PathLine;
import com.biblo.datastreamviewer.common.model.basic.PropertyObject;
import com.biblo.datastreamviewer.common.model.basic.SimpleText;
import com.biblo.datastreamviewer.linegraph.model.MultiPointLine;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.view.LineGraphRenderer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

import java.awt.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author gchapman
 * Implements the open gl version of the renderer for linegraph
 */
public class LineGraphLwjglRenderer extends LineGraphRenderer {

    private static final Logger mLogger = LogManager.getLogger();
    private static final int COLOR_LTGRAY = 0x7FCCCCCC;
    private static final int PROPERTY_LINE_PAINT = 0;
    private static final int PROPERTY_TEXT_FONT = 1;
    private static final int PROPERTY_TEXT_SPACING = 2;
    private static final int PROPERTY_TEXT_CENTER = 3;
    private static final int PROPERTY_TEXT_AWTFONT = 4;
    private static final int PROPERTY_TEXT_AWTCOLOR = 5;

    private PathLine mSimpleLine;
    private final static AtomicReference<Dimension> mCanvasDimension = new AtomicReference<Dimension>();
    private boolean mSetCanvas;
    private final com.biblo.datastreamviewer.linegraph.view.lwjgl.Paint mPaintLineDefault;
    private UnicodeFont mTextFontDefault;
    private final float mTextSpacingDefault;

    /**
     * Constructs the renderer with presenter.
     *
     * @param presenter Presenter of MVP design
     */
    public LineGraphLwjglRenderer(LineGraphPresenter presenter) {
        super(presenter);
        mSimpleLine = createLine();
        mSetCanvas = false;

        mPaintLineDefault = new com.biblo.datastreamviewer.linegraph.view.lwjgl.Paint();
        mPaintLineDefault.setStrokeWidth(0.8f);
        mPaintLineDefault.setAntiAlias(true);
        mPaintLineDefault.setColor(COLOR_LTGRAY);

        mTextSpacingDefault = 10f;
    }

    @Override
    protected PathLine createLine() {
        return new PathLineOpenGl();
    }

    @Override
    public void addLineProperties(PropertyObject object, int color, float width) {
        com.biblo.datastreamviewer.linegraph.view.lwjgl.Paint paint = (com.biblo.datastreamviewer.linegraph.view.lwjgl.Paint) object.getProperty(PROPERTY_LINE_PAINT);
        if(paint == null) {
            paint = new com.biblo.datastreamviewer.linegraph.view.lwjgl.Paint();
        }
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setStrokeWidth(width);
        object.putProperty(PROPERTY_LINE_PAINT, paint);
    }

    @Override
    public void addTextProperties(AttributableText textObject, int color, int size, String font, boolean bold, int alpha, int spacing, boolean centered) {
        Font awtFont = new Font(font, bold ? Font.BOLD : Font.PLAIN, size);
        Color awtColor = new Color(color);
        textObject.putProperty(PROPERTY_TEXT_AWTFONT, awtFont);
        textObject.putProperty(PROPERTY_TEXT_AWTCOLOR, awtColor);
        textObject.putProperty(PROPERTY_TEXT_CENTER, centered);
        textObject.putProperty(PROPERTY_TEXT_SPACING, Float.valueOf(spacing));
    }

    @Override
    protected void drawLine(MultiPointLine line) {
        final com.biblo.datastreamviewer.linegraph.view.lwjgl.Paint linePaint = (com.biblo.datastreamviewer.linegraph.view.lwjgl.Paint) line.getProperty(PROPERTY_LINE_PAINT);
        if (linePaint == null) { //must be specified
            return;
        }
        linePaint.apply();
        super.drawLine(line);
        GL11.glDisable(GL11.GL_BLEND);
    }

    @Override
    protected void drawLine(float startX, float startY, float endX, float endY, PropertyObject owner) {
        com.biblo.datastreamviewer.linegraph.view.lwjgl.Paint linePaint = (com.biblo.datastreamviewer.linegraph.view.lwjgl.Paint) owner.getProperty(PROPERTY_LINE_PAINT, mPaintLineDefault);
        mSimpleLine.reset();
        mSimpleLine.add(startX, startY);
        mSimpleLine.add(endX, endY);
        linePaint.apply();
        mSimpleLine.draw();
        GL11.glDisable(GL11.GL_BLEND);
    }

    @Override
    protected void drawText(SimpleText text, PropertyObject owner) {
        boolean centered = (Boolean) owner.getProperty(PROPERTY_TEXT_CENTER, false);
        UnicodeFont typeFont = getFont(owner);
        Float textSpacing = (Float) owner.getProperty(PROPERTY_TEXT_SPACING, mTextSpacingDefault);
        if(typeFont == null) {
            return;
        }

        float correctionX = 0;
        if(centered) {
            correctionX = typeFont.getWidth(text.getText()) / 2.0f;
        }

        //Manually scale, translate
        float scaleX = getScaleX();
        float scaleY = getScaleY();
        float pointX = getScreenX(text.getPoint().getX(), scaleX) + textSpacing - correctionX;
        float pointY = getScreenY(text.getPoint().getY(), scaleY) - textSpacing;

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        typeFont.drawString(pointX, pointY, text.getText());
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
    }

    @Override
    protected void drawScreenText(SimpleText text, PropertyObject owner) {
        boolean centered = (Boolean) owner.getProperty(PROPERTY_TEXT_CENTER, false);
        UnicodeFont typeFont = getFont(owner);
        Float textSpacing = (Float) owner.getProperty(PROPERTY_TEXT_SPACING, mTextSpacingDefault);
        if(typeFont == null) {
            return;
        }

        float correctionX = 0;
        if(centered) {
            correctionX = typeFont.getWidth(text.getText()) / 2.0f;
        }

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        typeFont.drawString(text.getPoint().getX() + textSpacing - correctionX,
                mDisplayPort.getTop() - text.getPoint().getY() - textSpacing, text.getText());
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
    }

    @Override
    protected boolean initRender() {
        boolean success = true;
        try {
            Display.create();
            mTextFontDefault = createFont(new Font("Calibri", Font.PLAIN, 10), Color.LIGHT_GRAY);
            success = mTextFontDefault != null;
        } catch (LWJGLException e) {
            error(e.getMessage());
        }
        return success;
    }

    protected UnicodeFont createFont(Font font, Color color) {
        UnicodeFont textFont = new UnicodeFont(font);
        textFont.getEffects().add(new ColorEffect(color));
        textFont.addAsciiGlyphs();
        textFont.addGlyphs(400, 600);
        try
        {
            textFont.loadGlyphs();
        }
        catch (SlickException e)
        {
            error(e.getMessage());
            return null;
        }
        return textFont;
    }

    protected UnicodeFont getFont(PropertyObject owner) {
        Font font = (Font) owner.getProperty(PROPERTY_TEXT_AWTFONT);
        Color color = (Color) owner.getProperty(PROPERTY_TEXT_AWTCOLOR);
        if(font != null && color != null) {
            //we create the font here, in the render loop because this thread is where lwjgl is created
            owner.putProperty(PROPERTY_TEXT_FONT, createFont(font, color));
            owner.removeProperty(PROPERTY_TEXT_AWTFONT); //no need for these now, created font
            owner.removeProperty(PROPERTY_TEXT_AWTCOLOR);
        }
        return (UnicodeFont) owner.getProperty(PROPERTY_TEXT_FONT, mTextFontDefault);
    }

    @Override
    protected boolean beginRender() {
        Dimension dimension = mCanvasDimension.getAndSet(null);
        if (dimension != null) {
            GL11.glMatrixMode(GL11.GL_PROJECTION);
            GL11.glLoadIdentity();
            GL11.glOrtho(0, dimension.getWidth(), dimension.getHeight(), 0, 0, 1);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            GL11.glMatrixMode(GL11.GL_MODELVIEW);
            GL11.glViewport(0, 0, dimension.width, dimension.height);
            mSetCanvas = true;
        }
        if(mSetCanvas) {
            GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // set background (clear) color
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT); // clear color
        }
        return mSetCanvas;
    }

    @Override
    protected void endRender() {
        if(mSetCanvas) {
            Display.update();
            Display.sync(60);
        }
    }

    @Override
    protected void finishRender() {
        Display.destroy();
    }

    @Override
    protected long getTimeNow() {
        return System.currentTimeMillis();
    }

    @Override
    protected void error(String message) {
        mLogger.error(message);
    }

    /**
     * Will set the dimension for the rendering
     * @param dimension
     */
    public void setDimension(Dimension dimension) {
        mCanvasDimension.set(dimension);
    }
}
