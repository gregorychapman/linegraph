package com.biblo.datastreamviewer.linegraph.view.lwjgl;

import com.biblo.datastreamviewer.common.model.ScaleAuditor;
import com.biblo.datastreamviewer.common.model.WorldAuditor;
import com.biblo.datastreamviewer.common.model.basic.*;
import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.Rectangle;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.view.LineGraphView;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * @author gchapman
 */
public class LineGraphCanvas extends Canvas implements LineGraphView {

    private final static float MIN_DIFFERENCE = 0.01f;

    private final WorldAuditor mWorldCheck;
    private final ScaleAuditor mScaleCheck;
    private boolean mSurfaceCreated;
    private boolean mPendingRun;
    private boolean mRunning;
    private Dimension mSize;
    private LineGraphLwjglRenderer mRenderer;
    private LineGraphPresenter mPresenter;
    private LineGraphLwjglInput mInput;
    private WritableRectangle mWorldBounds; //world coordinates
    private WritableRectangle mViewPort;    //world coordinates
    private WritableRectangle mDisplayPort; //pixels
    private WritablePoint mScreenResolution;

    public LineGraphCanvas() {
        setFocusable(true);
        requestFocus();
        setIgnoreRepaint(true);
        mSize = new Dimension();
        mRunning = false;
        mPendingRun = false;
        mSurfaceCreated = false;
        mDisplayPort = new WritableRectangle();
        mWorldBounds = new WritableRectangle();
        mScreenResolution = new WritablePoint();
        mWorldCheck = new WorldAuditor(mWorldBounds);
        mScaleCheck = new ScaleAuditor(MIN_DIFFERENCE);
        //TODO store/load view port
        mViewPort = new WritableRectangle(-100,-100, 10000, 502);

        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                onSizeChanged();
            }
        });
    }

    private void onSizeChanged() {
        getSize(mSize);
        mRenderer.setDimension(mSize); //handles resize of canvas
        //bottom-up
        mDisplayPort.set(0, getHeight(), getWidth(), 0);
        //For now assume 1 to 1 pixel with size
        mScreenResolution.setX(getWidth());
        mScreenResolution.setY(getHeight());
    }

    @Override
    public final void addNotify() {
        super.addNotify();
        canvasCreated();
    }

    @Override
    public final void removeNotify() {
        super.removeNotify();
        canvasDestroyed();
    }

    private void canvasCreated() {
        mSurfaceCreated = true;
        if(mPendingRun) { //if startRender was called before surface created, run
            startRender();
            mPendingRun = false;
        }
    }

    private void canvasDestroyed() {
        mSurfaceCreated = false;
        stopRender();
    }

    /**
     * View implementation
     */

    @Override
    public void startRender() {
        mPendingRun = !mSurfaceCreated;
        if(mSurfaceCreated && !mRunning) {
            try {
                Display.setParent(this);
                mRenderer.start();
                mRunning = true;
            }
            catch(LWJGLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void stopRender() {
        mRenderer.stop();
        mRunning = false;
    }

    @Override
    public void setPresenter(LineGraphPresenter presenter) {
        mPresenter = presenter;
        mRenderer = new LineGraphLwjglRenderer(mPresenter);
        mInput = new LineGraphLwjglInput(mPresenter);
        mRenderer.init();
    }

    @Override
    public void addLineProperties(PropertyObject object, int color, float width) {
        mRenderer.addLineProperties(object, color, width);
    }

    @Override
    public void addTextProperties(AttributableText textObject, int color, int size, String font, boolean bold, int alpha, int spacing, boolean centered) {
        mRenderer.addTextProperties(textObject, color, size, font, bold, alpha, spacing, centered);
    }

    @Override
    public void update(long deltaTimeMillis) {
        mInput.update(deltaTimeMillis, this);
        mWorldCheck.update(mViewPort);
        mScaleCheck.update(mViewPort);
    }

    @Override
    public Rectangle getViewPort(WritableRectangle viewPort) {
        viewPort.set(mViewPort);
        return viewPort;
    }

    @Override
    public Rectangle getDisplayPort(WritableRectangle displayPort) {
        displayPort.set(mDisplayPort);
        return displayPort;
    }

    @Override
    public void setWorldBounds(Rectangle worldBounds) {
        mWorldBounds.set(worldBounds);
    }

    @Override
    public void setViewPort(float left, float top, float right, float bottom) {
        mViewPort.set(left, top, right, bottom);
    }

    @Override
    public void setViewPort(float left, float bottom) {
        mViewPort.setX(left);
        mViewPort.setY(bottom);
    }

    @Override
    public boolean displayToView(float px, float py, WritablePoint viewPoint) {
        float pRatioX = (px - mDisplayPort.getLeft()) / mDisplayPort.getWidth();
        float pRatioY = (py - mDisplayPort.getBottom()) / mDisplayPort.getHeight();
        if(viewPoint != null) {
            viewPoint.setX(mViewPort.getLeft() + mViewPort.getWidth() * pRatioX);
            viewPoint.setY(mViewPort.getBottom() + mViewPort.getHeight() * pRatioY);
        }
        return mDisplayPort.contains(px, py);
    }

    @Override
    public boolean viewToDisplay(float x, float y, WritablePoint displayPoint) {
        float vRatioX = (x - mViewPort.getLeft()) / mViewPort.getWidth();
        float vRatioY = (y - mViewPort.getBottom()) / mViewPort.getHeight();
        float px = mDisplayPort.getLeft() + mDisplayPort.getWidth() * vRatioX;
        float py = mDisplayPort.getBottom() + mDisplayPort.getHeight() * vRatioY;
        if(displayPoint != null) {
            displayPoint.setX(px);
            displayPoint.setY(py);
        }
        return mDisplayPort.contains(px, py);
    }

    @Override
    public boolean displayToViewDistance(float originX, float originY, float spanX, float spanY, WritablePoint span) {
        if(span != null) {
            span.setX((spanX / mDisplayPort.getWidth()) * mViewPort.getWidth());
            span.setY((spanY / mDisplayPort.getHeight()) * mViewPort.getHeight());
        }
        return displayToView(originX, originY, null);
    }

    @Override
    public boolean viewToDisplayDistance(float originX, float originY, float spanX, float spanY, WritablePoint span) {
        if(span != null) {
            span.setX((spanX / mViewPort.getWidth()) * mDisplayPort.getWidth());
            span.setY((spanY / mViewPort.getHeight()) * mDisplayPort.getHeight());
        }
        return viewToDisplay(originX, originY, null);
    }

    @Override
    public Point getScreenResolution() {
        return mScreenResolution;
    }

    @Override
    public boolean onDown() {
        return mInput.onDown(this);
    }

    @Override
    public boolean onDoubleTap(float x, float y) {
        return mInput.onDoubleTap(x, y, this);
    }

    @Override
    public boolean onScaleBegin(float focusX, float focusY, float scaleFactor, float lastSpanX, float lastSpanY, float currentSpanX, float currentSpanY) {
        return mInput.onScaleBegin(focusX,
                focusY,
                scaleFactor,
                lastSpanX,
                lastSpanY,
                currentSpanX,
                currentSpanY,
                this);
    }

    @Override
    public boolean onScale(float focusX, float focusY, float scaleFactor, float lastSpanX, float lastSpanY, float currentSpanX, float currentSpanY) {
        return mInput.onScale(focusX,
                focusY,
                scaleFactor,
                lastSpanX,
                lastSpanY,
                currentSpanX,
                currentSpanY,
                this);
    }

    @Override
    public boolean onScaleEnd(float focusX, float focusY, float scaleFactor, float lastSpanX, float lastSpanY, float currentSpanX, float currentSpanY) {
        return mInput.onScaleEnd(focusX,
                focusY,
                scaleFactor,
                lastSpanX,
                lastSpanY,
                currentSpanX,
                currentSpanY,
                this);
    }

    @Override
    public boolean onScroll(float originX, float originY, float distanceX, float distanceY) {
        return mInput.onScroll(originX, originY, distanceX, distanceY, this);
    }

    @Override
    public boolean onFling(float originX, float originY, float velocityX, float velocityY) {
        return mInput.onFling(originX, originY, velocityX, velocityY, this);
    }
}
