package com.biblo.datastreamviewer.linegraph.view.lwjgl;

import com.biblo.datastreamviewer.common.model.basic.WritablePoint;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.view.LineGraphView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author gchapman
 * Implements the user input for desktop application
 */
public class LineGraphLwjglInput {

    private WritablePoint mZoomCenter;
    private WritablePoint mSavedPoint;
    private LineGraphPresenter mPresenter;
    private WritableRectangle mStartingViewPort;
    private WritablePoint mDistances;
    private LwjglMouse mMouseState;
    private MouseButtonListener mMouseButtonListener;
    private MouseMovementListener mMouseMovementListener;

    public LineGraphLwjglInput(LineGraphPresenter presenter) {
        mStartingViewPort = new WritableRectangle();
        mDistances = new WritablePoint();
        mSavedPoint = new WritablePoint();
        mPresenter = presenter;
        mMouseState = new LwjglMouse();
        mZoomCenter = new WritablePoint();
        mMouseMovementListener = new MouseMovementListener();
        mMouseButtonListener = new MouseButtonListener(mMouseMovementListener);
        mMouseState.addButtonListener(mMouseButtonListener);
        mMouseState.addMovementListener(mMouseMovementListener);
    }

    private class MouseButtonListener implements ActionListener {
        private MouseMovementListener mMovement;
        private LwjglMouse.LwjglMouseButton mPrimaryButton;

        public MouseButtonListener(MouseMovementListener movement) {
            mMovement = movement;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            LwjglMouse.LwjglMouseButton button = (LwjglMouse.LwjglMouseButton) e.getSource();
            if (mPrimaryButton == null) {
                if (button.isPressed()) {
                    mPrimaryButton = button;
                }
            } else {
                if (mPrimaryButton.getId() == button.getId() && !button.isPressed()) {
                    mPrimaryButton = null;
                }
            }

            boolean primaryButtonSet = mPrimaryButton != null;
            mMovement.setDragged(primaryButtonSet);
            mMovement.setButton(mPrimaryButton);
            if (primaryButtonSet && mPrimaryButton.getId() == button.getId()) {
                mSavedPoint.setX(mPrimaryButton.getX());
                mSavedPoint.setY(mPrimaryButton.getY());
                mPresenter.onDown();
            }
        }

        /**
         * @return Primary button pressed or null
         */
        public LwjglMouse.LwjglMouseButton getPrimaryButton() {
            return mPrimaryButton;
        }
    }

    private class MouseMovementListener implements ActionListener {
        private static final int MOUSE_LEFT_BUTTON = 0;
        private static final float MAX_WHEEL_CHANGE = 5;
        private boolean mDragged;
        private LwjglMouse.LwjglMouseButton mButton;

        @Override
        public void actionPerformed(ActionEvent e) {
            LwjglMouse mouse = (LwjglMouse) e.getSource();
            switch(e.getID()) {
                case LwjglMouse.MOUSE_MOVEMENT_ID:
                    handleMouseMove(mouse);
                    break;
                case LwjglMouse.MOUSE_WHEEL_ID:
                    handleMouseWheel(mouse);
                    break;
            }
        }

        private void handleMouseWheel(LwjglMouse mouse) {
            if(mouse.getWheel() != 0) {
                float factor = (1.0f - (Math.min(mouse.getWheel(), MAX_WHEEL_CHANGE) / MAX_WHEEL_CHANGE));
                if(mPresenter.onScaleBegin(mouse.getX(), mouse.getY(), factor, factor, factor, 1, 1)) {
                    mPresenter.onScale(mouse.getX(), mouse.getY(), factor, factor, factor, 1, 1);
                    mPresenter.onScaleEnd(mouse.getX(), mouse.getY(), factor, factor, factor, 1, 1);
                }
            }
        }

        private void handleMouseMove(LwjglMouse mouse) {
            if(mDragged && mButton != null && mButton.getId() == MOUSE_LEFT_BUTTON) {
                int dx = mouse.getX() - (int)mSavedPoint.getX();
                int dy = mouse.getY() - (int)mSavedPoint.getY();
                mPresenter.onScroll(mSavedPoint.getX(), mSavedPoint.getY(), dx, dy);
                mSavedPoint.setX(mouse.getX());
                mSavedPoint.setY(mouse.getY());
            }
        }

        /**
         * Sets the dragged state
         * @param dragged True if dragging is in progress
         */
        public void setDragged(boolean dragged) {
            mDragged = dragged;
        }

        /**
         * @return True if dragging
         */
        public boolean isDragged() {
            return mDragged;
        }

        /**
         * Sets the button for dragging
         * @param button Null or button set primary
         */
        public void setButton(LwjglMouse.LwjglMouseButton button) {
            mButton = button;
        }
    }

    private void scroll(float distanceX, float distanceY, LineGraphView view) {
        //negate y for bottom up
        view.displayToViewDistance(0, 0, -distanceX, -distanceY, mDistances);
        view.getViewPort(mStartingViewPort);
        mStartingViewPort.addX(mDistances.getX());
        mStartingViewPort.addY(mDistances.getY());
        //Only shift the bottom left for scrolling, no need to change all coordinates
        view.setViewPort(mStartingViewPort.getLeft(), mStartingViewPort.getBottom());
    }

    private boolean scroll(float originX, float originY, float distanceX, float distanceY, LineGraphView view) {
        //negate y for bottom up
        if(view.displayToViewDistance(originX, originY, -distanceX, -distanceY, mDistances)) {
            view.getViewPort(mStartingViewPort);
            mStartingViewPort.addX(mDistances.getX());
            mStartingViewPort.addY(mDistances.getY());
            //Only shift the bottom left for scrolling, no need to change all coordinates
            view.setViewPort(mStartingViewPort.getLeft(), mStartingViewPort.getBottom());
            return true;
        }
        return false;
    }

    private void zoom(float scale, LineGraphView view) {
        zoom(scale, scale, view);
    }

    private void zoom(float scaleX, float scaleY, LineGraphView view) {
        float updateWidth = scaleX * mStartingViewPort.getWidth();
        float updateHeight = scaleY * mStartingViewPort.getHeight();
        float normalizedX = (mZoomCenter.getX() - mStartingViewPort.getLeft()) /
                mStartingViewPort.getWidth();
        float normalizedY = (mZoomCenter.getY() - mStartingViewPort.getBottom()) /
                mStartingViewPort.getHeight();

        view.setViewPort(mZoomCenter.getX() - updateWidth * normalizedX, //left
                mZoomCenter.getY() + updateHeight * (1 - normalizedY), //top
                mZoomCenter.getX() + updateWidth * (1 - normalizedX), //right
                mZoomCenter.getY() - updateHeight * normalizedY); //bottom
    }

    /**
     * Handles the on down gesture
     * @param view View of MVP
     * @return True if consumed
     */
    public boolean onDown(LineGraphView view) {
        view.getViewPort(mStartingViewPort);
        return true;
    }

    /**
     * Handles a double tap gesture
     * @param px Pixel X in screen coordinates
     * @param py Pixel Y in screen coordinates
     * @param view View of MVP
     * @return True if consumed
     */
    public boolean onDoubleTap(float px, float py, LineGraphView view) {
        return false;
    }

    /**
     * Event for begin scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @param view View to scale
     * @return True if consumed, false if not
     */
    public boolean onScaleBegin(float focusX, float focusY, float scaleFactor, float lastSpanX, float lastSpanY, float currentSpanX, float currentSpanY, LineGraphView view) {
        view.getViewPort(mStartingViewPort);
        return view.displayToView(focusX, focusY, mZoomCenter);
    }

    /**
     * Event for scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @param view View to scale
     * @return True if consumed, false if not
     */
    public boolean onScale(float focusX, float focusY, float scaleFactor, float lastSpanX, float lastSpanY, float currentSpanX, float currentSpanY, LineGraphView view) {
        zoom(lastSpanX/currentSpanX, lastSpanY/currentSpanY, view);
        return true;
    }

    /**
     * Event for end scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @param view View to scale
     * @return True if consumed, false if not
     */
    public boolean onScaleEnd(float focusX, float focusY, float scaleFactor, float lastSpanX, float lastSpanY, float currentSpanX, float currentSpanY, LineGraphView view) {
        return false;
    }

    /**
     * Handles a scroll gesture
     * @param originX origin of scroll in x
     * @param originY origin of scroll in y
     * @param distanceX Distance to handle in x
     * @param distanceY Distance to handle in y
     * @return True if consumed, false if not
     */
    public boolean onScroll(float originX, float originY, float distanceX, float distanceY, LineGraphView view) {
        return scroll(originX, originY, distanceX, distanceY, view);
    }

    /**
     * Handles a fling gesture
     * @param originX origin of scroll in x
     * @param originY origin of scroll in y
     * @param velocityX Fling velocity in X
     * @param velocityY Fling velocity in Y
     * @return True if consumed, false if not
     */
    public boolean onFling(float originX, float originY, float velocityX, float velocityY, LineGraphView view) {
        return false;
    }

    /**
     * Will update animation of the inputs
     * @param deltaTimeMillis Delta since last update, milliseconds
     * @param view View of MVP
     */
    public void update(long deltaTimeMillis, LineGraphView view) {
        mMouseState.update();
    }
}
