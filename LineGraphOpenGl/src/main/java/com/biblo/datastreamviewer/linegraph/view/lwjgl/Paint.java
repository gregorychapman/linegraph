package com.biblo.datastreamviewer.linegraph.view.lwjgl;

import org.lwjgl.opengl.GL11;

/**
 * @author gchapman
 * Contains open gl calls to set up paint for drawable objects
 */
public class Paint {

    protected int mColor;
    protected float mColorA;
    protected float mColorR;
    protected float mColorB;
    protected float mColorG;
    protected float mLineWidth;
    protected boolean mAntiAlias;

    /**
     * Apply the paint to lwjgl calls
     */
    public void apply() {
        GL11.glColor4f(mColorR, mColorG, mColorB, mColorA);
        GL11.glLineWidth (mLineWidth);
        if(mAntiAlias) {
            GL11.glEnable(GL11.GL_LINE_SMOOTH);
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);
        }
        else {
            GL11.glDisable(GL11.GL_LINE_SMOOTH);
            GL11.glDisable(GL11.GL_BLEND);
        }
    }

    public int getColor() {
        return mColor;
    }

    public void setColor(int color) {
        mColor = color;
        mColorA = normalizeColor((color & 0xFF000000) >>> 24);
        mColorR = normalizeColor((color & 0x00FF0000) >>> 16);
        mColorG = normalizeColor((color & 0x0000FF00) >>> 8);
        mColorB = normalizeColor(color & 0x000000FF);
    }

    private float normalizeColor(int i) {
        return i/255.0f;
    }

    public float getStrokeWidth() {
        return mLineWidth;
    }

    public void setStrokeWidth(float width) {
        mLineWidth = width;
    }

    public boolean getAntiAlias() {
        return mAntiAlias;
    }

    public void setAntiAlias(boolean antiAlias) {
        mAntiAlias = antiAlias;
    }
}
