package com.biblo.datastreamviewer.linegraph.view.lwjgl;

import com.biblo.datastreamviewer.common.model.basic.PathLine;
import com.biblo.datastreamviewer.common.model.basic.WritablePoint;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;

/**
 * @author gchapman
 * Represents a line strip path for lwjgl
 */
public class PathLineOpenGl implements PathLine {
    private ArrayList<WritablePoint> mBuffer;
    private int mCount;

    /**
     * Constructor
     */
    public PathLineOpenGl() {
        mBuffer = new ArrayList<WritablePoint>();
    }

    /**
     * Clears out the memory allocations
     */
    @Override
    public void clear() {
        mBuffer.clear();
        reset();
    }

    /**
     * Reset will not clear out previous memory allocations, which is faster way to remove
     * previous drawing if previous memory allocation isn't a concern.
     */
    @Override
    public void reset() {
        mCount = 0;
    }

    /**
     * Add a new point to the path
     * @param x X value
     * @param y Y value
     */
    @Override
    public void add(float x, float y) {
        ++mCount;
        if(mBuffer.size() < mCount) {
            mBuffer.add(new WritablePoint(x,y));
        }
        else {
            mBuffer.get(mCount-1).setX(x);
            mBuffer.get(mCount-1).setY(y);
        }
    }

    /**
     * Draws the path line
     */
    @Override
    public void draw() {
        GL11.glBegin(GL11.GL_LINE_STRIP);
        for(int i = 0; i < mCount && i < mBuffer.size(); ++i) {
            GL11.glVertex2f(mBuffer.get(i).getX(), mBuffer.get(i).getY());
        }
        GL11.glEnd();
    }
}
