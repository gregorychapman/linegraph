package com.biblo.datastreamviewer.linegraph.view.lwjgl;

import org.lwjgl.input.Mouse;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @author gchapman
 * Wraps the lwjgl mouse for easy use
 */
public class LwjglMouse {

    protected ArrayList<ActionListener> mButtonActions;
    protected ArrayList<LwjglMouseButton> mMouseButtons;
    protected ArrayList<ActionListener> mMovementActions;
    protected ActionEvent mMovementEvent;
    protected ActionEvent mWheelEvent;
    protected boolean mInitialized;

    public static final int DWHEEL_INCREMENT = 120;
    public static final int MOUSE_BUTTON_ID = 0;
    public static final int MOUSE_MOVEMENT_ID = 1;
    public static final int MOUSE_WHEEL_ID = 2;

    public final static String MOUSE_BUTTON = "MOUSE_BUTTON";
    public static final String MOUSE_MOVEMENT = "MOUSE_MOVEMENT";
    public static final String MOUSE_WHEEL = "MOUSE_WHEEL";

    /**
     * Sets up a mouse object
     */
    public LwjglMouse() {
        mInitialized = false;
        mMovementActions = new ArrayList<ActionListener>();
        mButtonActions = new ArrayList<ActionListener>();
        mMouseButtons = new ArrayList<LwjglMouseButton>();
        mMovementEvent = new ActionEvent(this, MOUSE_MOVEMENT_ID, MOUSE_MOVEMENT);
        mWheelEvent = new ActionEvent(this, MOUSE_WHEEL_ID, MOUSE_WHEEL);
    }

    /**
     * Must call this before using in the render thread
     */
    protected void initialize() {
        if(mInitialized) {
            return;
        }
        mInitialized = true;
        int count = Mouse.getButtonCount();
        for(int i = 0; i < count; ++i) {
            mMouseButtons.add(new LwjglMouseButton(i));
        }
    }

    /**
     * Call to update the mouse events
     */
    public void update() {
        initialize();
        while(Mouse.next()) {
            if(Mouse.getEventButton() < mMouseButtons.size() && Mouse.getEventButton() > -1) {
                mMouseButtons.get(Mouse.getEventButton()).update();
            }
            checkMovement();
            checkWheel();
        }
    }

    protected void checkWheel() {
        //detect if no movement
        if(Mouse.getEventDWheel() == 0) {
            return;
        }
        fireMovementEvent(mWheelEvent);
    }

    protected void checkMovement() {
        //detect if no movement
        if(Mouse.getEventDY() == 0 &&
           Mouse.getEventDX() == 0) {
            return;
        }
        fireMovementEvent(mMovementEvent);
    }

    /**
     * @return Get the current x position
     */
    public int getX() {
        return Mouse.getEventX();
    }

    /**
     * @return Get the current y position
     */
    public int getY() {
        return Mouse.getEventY();
    }

    /**
     * @return Get the current dx movement
     */
    public int getDx() {
        return Mouse.getEventDX();
    }

    /**
     * @return Get the current dy movement
     */
    public int getDy() {
        return Mouse.getEventDY();
    }

    /**
     * @return Get the current dwheel movement
     */
    public int getWheel() {
        return Mouse.getEventDWheel() / DWHEEL_INCREMENT;
    }

    /**
     * Add a listener to mouse movement
     * @param listener Listens to movement
     */
    public void addMovementListener(ActionListener listener) {
        mMovementActions.add(listener);
    }

    /**
     * Remove a listener from mouse movement
     * @param listener Listens to movement
     */
    public void removeMovementListener(ActionListener listener) {
        mMovementActions.remove(listener);
    }

    protected void fireMovementEvent(ActionEvent event) {
        for(ActionListener listener : mMovementActions) {
            listener.actionPerformed(event);
        }
    }

    /**
     * Add a listener to mouse buttons
     * @param listener Listens to buttons
     */
    public void addButtonListener(ActionListener listener) {
        mButtonActions.add(listener);
    }

    /**
     * Remove a listener from mouse buttons
     * @param listener Listens to buttons
     */
    public void removeButtonListener(ActionListener listener) {
        mButtonActions.remove(listener);
    }

    protected void fireButtonEvent(ActionEvent event) {
        for(ActionListener listener : mButtonActions) {
            listener.actionPerformed(event);
        }
    }

    protected class LwjglMouseButton {
        protected boolean mPressed;
        protected int mId;
        protected ActionEvent mPressedEvent;

        /**
         * Creates a button object
         * @param id Id of the button to watch
         */
        public LwjglMouseButton(int id) {
            mId = id;
            mPressedEvent = new ActionEvent(this, MOUSE_BUTTON_ID, MOUSE_BUTTON);
        }

        /**
         * Update button events
         */
        public void update() {
            if(Mouse.getEventButton() != mId) {
                return;
            }

            boolean pressed = Mouse.getEventButtonState();
            if((!mPressed && pressed) || (mPressed && !pressed)) {
                mPressed = pressed;
                fireButtonEvent(mPressedEvent);
            }
        }

        /**
         * @return ID of the button
         */
        public int getId() {
            return mId;
        }

        /**
         * @return True if pressed
         */
        public boolean isPressed() {
            return mPressed;
        }

        /**
         * @return X coordinate of button press
         */
        public int getX() {
            return Mouse.getEventX();
        }

        /**
         * @return Y coordinate of button press
         */
        public int getY() {
            return Mouse.getEventY();
        }
    }
}
