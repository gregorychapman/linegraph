//import com.jogamp.opengl.util.FPSAnimator;
//
//import javax.media.lwjgl.GL2;
//import javax.media.lwjgl.GLAutoDrawable;
//import javax.media.lwjgl.GLEventListener;
//import javax.media.lwjgl.awt.GLCanvas;
//import javax.media.lwjgl.awt.GLJPanel;
//import javax.media.lwjgl.glu.GLU;
//import javax.swing.*;
//import java.awt.*;
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
//import static javax.media.lwjgl.GL.*;  // GL constants
//import static javax.media.lwjgl.GL2.*; // GL2 constants
//
//public class LineGraphPanel extends GLJPanel implements GLEventListener {
//
//    // Define constants for the top-level container
//    private static String TITLE = "JOGL 2.0 Setup (GLCanvas)";  // window's title
//    private static final int CANVAS_WIDTH = 640;  // width of the drawable
//    private static final int CANVAS_HEIGHT = 480; // height of the drawable
//    private static final int FPS = 60; // animator's target frames per second
//
//    public static void main(String[] args) {
//        // Run the GUI codes in the event-dispatching thread for thread safety
//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
//                // Create the OpenGL rendering canvas
//                GLJPanel canvas = new LineGraphPanel();
//                canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
//
//                // Create a animator that drives canvas' display() at the specified FPS.
//                final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
//
//                // Create the top-level container
//                final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
//                frame.getContentPane().add(canvas);
//                frame.addWindowListener(new WindowAdapter() {
//                    @Override
//                    public void windowClosing(WindowEvent e) {
//                        // Use a dedicate thread to run the stop() to ensure that the
//                        // animator stops before program exits.
//                        new Thread() {
//                            @Override
//                            public void run() {
//                                if (animator.isStarted()) animator.stop();
//                                System.exit(0);
//                            }
//                        }.start();
//                    }
//                });
//                frame.setTitle(TITLE);
//                frame.pack();
//                frame.setVisible(true);
//                animator.start(); // start the animation loop
//            }
//        });
//    }
//
//    private GLU glu;  // for the GL Utility
//
//    /** Constructor to setup the GUI for this Component */
//    public LineGraphPanel() {
//        this.addGLEventListener(this);
//    }
//
//    // ------ Implement methods declared in GLEventListener ------
//
//    /**
//     * Called back immediately after the OpenGL context is initialized. Can be used
//     * to perform one-time initialization. Run only once.
//     */
//    @Override
//    public void init(GLAutoDrawable drawable) {
//        GL2 gl = drawable.getGL().getGL2();      // get the OpenGL graphics context
//        glu = new GLU();                         // get GL Utilities
//        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // set background (clear) color
//        gl.glClearDepth(1.0f);      // set clear depth value to farthest
////        gl.glEnable(GL_DEPTH_TEST); // enables depth testing
//        gl.glEnable(GL_LINE_SMOOTH); // enables depth testing
//        gl.glEnable (GL_BLEND);
//        gl.glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//        gl.glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);
////        gl.glDepthFunc(GL_LEQUAL);  // the type of depth test to do
////        gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // best perspective correction
////        gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out lighting
//
//        // ----- Your OpenGL initialization code here -----
//    }
//
//    /**
//     * Call-back handler for window re-size event. Also called when the drawable is
//     * first set to visible.
//     */
//    @Override
//    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
//        GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
//
////        if (height == 0) height = 1;   // prevent divide by zero
////        float aspect = (float)width / height;
////
////        // Set the view port (display area) to cover the entire window
////        gl.glViewport(0, 0, width, height);
////
////        // Setup perspective projection, with aspect ratio matches viewport
////        gl.glMatrixMode(GL_PROJECTION);  // choose projection matrix
////        gl.glLoadIdentity();             // reset projection matrix
////        glu.gluPerspective(45.0, aspect, 0.1, 100.0); // fovy, aspect, zNear, zFar
////
////        // Enable the model-view transform
////        gl.glMatrixMode(GL_MODELVIEW);
////        gl.glLoadIdentity(); // reset
//
//        gl.glLineWidth (0.5f);
//
//        gl.glMatrixMode (GL_PROJECTION);
//
//        gl.glLoadIdentity ();
//
//        gl.glOrtho (0, width, height, 0, 0, 1);
//
//        gl.glDisable(GL_DEPTH_TEST);
//
//        gl.glMatrixMode (GL_MODELVIEW);
//
//        gl.glLoadIdentity();
//
//    }
//
//    /**
//     * Called back by the animator to perform rendering.
//     */
//    @Override
//    public void display(GLAutoDrawable drawable) {
//        GL2 gl = drawable.getGL().getGL2();  // get the OpenGL 2 graphics context
//
//        gl.glLoadIdentity();
//        gl.glTranslatef(0.375f, 0.375f, 0);
//
//        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear color and depth buffers
//
//
//
//        // gl.glLoadIdentity();  // reset the model-view matrix
//
//        // ----- Your OpenGL rendering code here (Render a white triangle for testing) -----
//     //   gl.glTranslatef(0.0f, 0.0f, -3.0f); // translate into the screen
////        gl.glBegin(GL_TRIANGLES); // draw using triangles
////        gl.glVertex3f(0.0f, 1.0f, 0.0f);
////        gl.glVertex3f(-1.0f, -1.0f, 0.0f);
////        gl.glVertex3f(1.0f, -1.0f, 0.0f);
//
//        gl.glBegin(GL_LINE_STRIP); // draw using triangles
//        gl.glVertex2f(0,0);
//        gl.glVertex2f(600f, 40f);
//        gl.glVertex2f(300f, 450f);
//        gl.glEnd();
//    }
//
//    /**
//     * Called back before the OpenGL context is destroyed. Release resource such as buffers.
//     */
//    @Override
//    public void dispose(GLAutoDrawable drawable) { }
//
//}


import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.WritablePoint;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;
import com.biblo.datastreamviewer.linegraph.model.MultiPointLine;
import com.biblo.datastreamviewer.linegraph.model.SimpleLineGraphModel;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.presenter.ThreadSafeLineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.view.lwjgl.LineGraphCanvas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;
import java.util.TimerTask;
import java.util.Timer;

public class LineGraphFrameTest extends JFrame {
    public static final int DATA_LINE_COLOR = 0xFFFF0000;
    public static final float DATA_LINE_WIDTH = 1.5f;
    private static final float TITLE_Y_OFFSET = 150;

    private LineGraphCanvas mLineGraphView;
    private ThreadSafeLineGraphPresenter mLineGraphPresenter;
    private SimpleLineGraphModel mLineGraphModel;
    private MultiPointLine mLine;
    private Timer mTimer;
    private WritablePoint mTitleLocation;
    private LineGraphPresenter.TextProperties mTitleProperties;
    private AttributableText mTitle;
    private boolean mTitleLocationSet;
    private WritableRectangle mDisplayPort;

    public void init() {
        setLayout(new BorderLayout());

        try {
            mLineGraphView = new LineGraphCanvas();
            mLineGraphModel = new SimpleLineGraphModel();
            mLineGraphPresenter = new ThreadSafeLineGraphPresenter(mLineGraphModel, mLineGraphView);
            mLineGraphView.setPresenter(mLineGraphPresenter);
            mLine = mLineGraphPresenter.addDataLine(new LineGraphPresenter.LineProperties(
                    DATA_LINE_COLOR, DATA_LINE_WIDTH)); //single red line

            mDisplayPort = new WritableRectangle();
            mTitleLocation = new WritablePoint();
            mTitleProperties = new LineGraphPresenter.TextProperties(0x33FFC800, 35, "Calibri");
            mTitleProperties.centered = true;
            mTitle = mLineGraphPresenter.addText("Title", mTitleLocation, mTitleProperties);

            add(mLineGraphView);
        } catch (Exception e) {
            System.err.println(e);
            throw new RuntimeException("Unable to create display");
        }

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                mLineGraphPresenter.stop();
                System.exit(0);
            }
        });

        mLineGraphView.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                onSizeChanged();
            }
        });
    }

    private void onSizeChanged() {
        setupTitleLocation();
    }

    public void start() {
        mLineGraphPresenter.start();
        setVisible(true);

        final float[] data = new float[6000];
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                Random r = new Random();
                for(int i = 0; i < data.length; ++i) {
                    data[i] = r.nextFloat() * 100;
                }
                mLineGraphPresenter.onData(mLine, data);

            }
        }, 10, 10);
    }

    private void setupTitleLocation() {
        mLineGraphPresenter.getDisplayPort(mDisplayPort);
        mTitleLocation.setX(mDisplayPort.getLeft() + mDisplayPort.getWidth() / 2);
        mTitleLocation.setY(mDisplayPort.getTop() - TITLE_Y_OFFSET);
        mLineGraphPresenter.onText(mTitle, "Title", mTitleLocation, mTitleProperties);
    }

    public void stop() {

    }

    public static void main(String[] argv) {
        LineGraphFrameTest displayExample = new LineGraphFrameTest();
        displayExample.setSize(400,400);
        displayExample.init();
        displayExample.start();
    }
}