package com.biblo.datastreamviewer.linegraph.view.android;

import android.content.Context;
import android.os.SystemClock;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.OverScroller;

import com.biblo.datastreamviewer.common.model.basic.WritablePoint;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.view.LineGraphView;

/**
 * @author gchapman
 * Will detect and forward on gestures from the view to the presenter
 */
public class LineGraphGestureInput {

    private static final String TAG = LineGraphGestureInput.class.getSimpleName();
    private static final float ZOOM_GOAL = 0.25f;
    private static final float FLING_DISTANCE = 3000;

    private LineGraphPresenter mPresenter;
    private GestureDetectorCompat mGestureDetector;
    private ScaleGestureDetector mScaleGestureDetector;
    private OverScroller mScroller;
    private TapZoom mTapZoom;
    private WritablePoint mDistances;
    private WritablePoint mZoomCenter;
    private WritableRectangle mStartingViewPort;
    private WritablePoint mFlingOrigin;
    private WritablePoint mFlingDistances;

    private final GestureDetector.SimpleOnGestureListener mGestureListener =
        new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                    float distanceX, float distanceY) {

                return mPresenter.onScroll(e1.getX(), e1.getY(), distanceX, distanceY);
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return mPresenter.onFling(e1.getX(), e1.getY(), velocityX, velocityY);
            }

            @Override
            public boolean onDown(MotionEvent event) {
                return mPresenter.onDown();
            }

            @Override
            public boolean onDoubleTap(MotionEvent event) {
                return mPresenter.onDoubleTap(event.getX(), event.getY());
            }
        };

    private final ScaleGestureDetector.OnScaleGestureListener mScaleGestureListener =
        new ScaleGestureDetector.OnScaleGestureListener() {
            private boolean isNegative(float number) {
                return number < 0;
            }

            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                //need the spans to have the same sign!
                if(isNegative(detector.getPreviousSpanX()) != isNegative(detector.getCurrentSpanX()) ||
                   isNegative(detector.getPreviousSpanY()) != isNegative(detector.getCurrentSpanY()) ||
                    detector.getCurrentSpanX() == 0 ||
                    detector.getCurrentSpanY() == 0) {
                    return true;
                }

                return mPresenter.onScale(detector.getFocusX(),
                                          detector.getFocusY(),
                                          detector.getScaleFactor(),
                                          detector.getPreviousSpanX(),
                                          detector.getPreviousSpanY(),
                                          detector.getCurrentSpanX(),
                                          detector.getCurrentSpanY());
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                return mPresenter.onScaleBegin(detector.getFocusX(),
                                               detector.getFocusY(),
                                               detector.getScaleFactor(),
                                               detector.getPreviousSpanX(),
                                               detector.getPreviousSpanY(),
                                               detector.getCurrentSpanX(),
                                               detector.getCurrentSpanY());
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
                mPresenter.onScaleEnd(detector.getFocusX(),
                                      detector.getFocusY(),
                                      detector.getScaleFactor(),
                                      detector.getPreviousSpanX(),
                                      detector.getPreviousSpanY(),
                                      detector.getCurrentSpanX(),
                                      detector.getCurrentSpanY());
            }
        };

    /**
     * Handles the double tap zoom animation
     */
    private static class TapZoom {
        private Interpolator mInterpolator; //for smooth animation
        private int mAnimationDurationMillis; //animation duration
        private boolean mComplete; //animation completed
        private float mCurrentValue; //current value for zoom
        private float mGoalValue; //end with this value
        private long mStartTime; //start of animation time

        /**
         * Construct double tap zoom
         * @param context Android context that owns zoom
         */
        public TapZoom(Context context) {
            mComplete = true;
            mAnimationDurationMillis = context.getResources().getInteger(android.R.integer.config_shortAnimTime);
            mInterpolator = new DecelerateInterpolator();
        }

        /**
         * Will start the animation with an end goal
         * @param goal End goal value
         */
        public void start(float goal) {
            mComplete = false;
            mGoalValue = goal;
            mCurrentValue = 1.0f;
            mStartTime = SystemClock.elapsedRealtime();
        }

        /**
         * Will update the animation, call periodically
         * @return True if animation continues
         */
        public boolean update() {
            if(mComplete) {
                return false;
            }

            //check if animation is completed
            float deltaTime = SystemClock.elapsedRealtime() - mStartTime;
            mComplete = deltaTime > mAnimationDurationMillis;
            if(mComplete) {
                mCurrentValue = mGoalValue;
                return true; //last one, make it count
            }

            mCurrentValue = mGoalValue * mInterpolator.getInterpolation(deltaTime / mAnimationDurationMillis);
            return true;
        }

        /**
         * @return Current value
         */
        public float getValue() {
            return mCurrentValue;
        }

        /**
         * Will stop the animation
         */
        public void stop() {
            mComplete = true; //stop animation
        }
    }

    /**
     * Constructs the user input capture for the LineGraph Android platform
     * @param context Android context that owns input
     * @param presenter Presenter of MVP
     */
    public LineGraphGestureInput(Context context, LineGraphPresenter presenter) {
        mPresenter = presenter;
        mGestureDetector = new GestureDetectorCompat(context, mGestureListener);
        mScaleGestureDetector = new ScaleGestureDetector(context, mScaleGestureListener);
        mScroller = new OverScroller(context);
        mTapZoom = new TapZoom(context);
        mZoomCenter = new WritablePoint();
        mStartingViewPort = new WritableRectangle();
        mDistances = new WritablePoint();
        mFlingOrigin = new WritablePoint();
        mFlingDistances = new WritablePoint();
    }

    /**
     * Handles a touch event
     * @param event Touch event that occurred
     * @return True if consumed
     */
    public boolean onTouchEvent(MotionEvent event) {
        //IMPORTANT: DO NOT short circuit this logic. Both scale and gesture need to be called.
        //If short circuit, the panning/scaling will have lag because of missing events
        return mGestureDetector.onTouchEvent(event) | mScaleGestureDetector.onTouchEvent(event);
    }

    /**
     * Will update animation of the inputs
     * @param deltaTimeMillis Delta since last update, milliseconds
     * @param view View of MVP
     */
    public void update(long deltaTimeMillis, LineGraphView view) {
        //Update zoom animation
        tapZoom(view);
        fling(view);
    }

    private boolean scroll(float originX, float originY, float distanceX, float distanceY, LineGraphView view) {
        //negate y for bottom up
        if(view.displayToViewDistance(originX, originY, distanceX, -distanceY, mDistances)) {
            view.getViewPort(mStartingViewPort);
            mStartingViewPort.addX(mDistances.getX());
            mStartingViewPort.addY(mDistances.getY());
            //Only shift the bottom left for scrolling, no need to change all coordinates
            view.setViewPort(mStartingViewPort.getLeft(), mStartingViewPort.getBottom());
            return true;
        }
        return false;
    }

    private void scroll(float distanceX, float distanceY, LineGraphView view) {
        //negate y for bottom up
        view.displayToViewDistance(0, 0, distanceX, -distanceY, mDistances);
        view.getViewPort(mStartingViewPort);
        mStartingViewPort.addX(mDistances.getX());
        mStartingViewPort.addY(mDistances.getY());
        //Only shift the bottom left for scrolling, no need to change all coordinates
        view.setViewPort(mStartingViewPort.getLeft(), mStartingViewPort.getBottom());
    }

    private void zoom(float scale, LineGraphView view) {
        zoom(scale, scale, view);
    }

    private void zoom(float scaleX, float scaleY, LineGraphView view) {
        float updateWidth = scaleX * mStartingViewPort.getWidth();
        float updateHeight = scaleY * mStartingViewPort.getHeight();
        float normalizedX = (mZoomCenter.getX() - mStartingViewPort.getLeft()) /
                mStartingViewPort.getWidth();
        float normalizedY = (mZoomCenter.getY() - mStartingViewPort.getBottom()) /
                mStartingViewPort.getHeight();

        view.setViewPort(mZoomCenter.getX() - updateWidth * normalizedX, //left
                mZoomCenter.getY() + updateHeight * (1 - normalizedY), //top
                mZoomCenter.getX() + updateWidth * (1 - normalizedX), //right
                mZoomCenter.getY() - updateHeight * normalizedY); //bottom
    }

    private void tapZoom(LineGraphView view) {
        if(mTapZoom.update()) {
            float updateZoom = (1f - mTapZoom.getValue());
            zoom(updateZoom, view);
        }
    }

    private void fling(LineGraphView view) {
        if(mScroller.computeScrollOffset()){
            float deltaX = mFlingOrigin.getX() - mScroller.getCurrX();
            float deltaY = mFlingOrigin.getY() - mScroller.getCurrY();
            mFlingOrigin.setX(mScroller.getCurrX());
            mFlingOrigin.setY(mScroller.getCurrY());
            scroll(deltaX, deltaY, view);
        }
    }

    /**
     * Handles a double tap gesture
     * @param px Pixel X in screen coordinates
     * @param py Pixel Y in screen coordinates
     * @param view View of MVP
     * @return True if consumed
     */
    public boolean onDoubleTap(float px, float py, LineGraphView view) {
        mTapZoom.stop();
        if(view.displayToView(px, py, mZoomCenter)) {
            mTapZoom.start(ZOOM_GOAL);
            return true;
        }
        return false;
    }

    /**
     * Handles the on down gesture
     * @param view View of MVP
     * @return True if consumed
     */
    public boolean onDown(LineGraphView view) {
        mScroller.forceFinished(true);
        view.getViewPort(mStartingViewPort);
        return true;
    }

    /**
     * Event for begin scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @param view View to scale
     * @return True if consumed, false if not
     */
    public boolean onScaleBegin(float focusX,
                                float focusY,
                                float scaleFactor,
                                float lastSpanX,
                                float lastSpanY,
                                float currentSpanX,
                                float currentSpanY,
                                LineGraphView view) {
        mScroller.forceFinished(true);
        view.getViewPort(mStartingViewPort);
        return view.displayToView(focusX, focusY, mZoomCenter);
    }

    /**
     * Event for scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @param view View to scale
     * @return True if consumed, false if not
     */
    public boolean onScale(float focusX,
                           float focusY,
                           float scaleFactor,
                           float lastSpanX,
                           float lastSpanY,
                           float currentSpanX,
                           float currentSpanY,
                           LineGraphView view) {
        zoom(lastSpanX/currentSpanX, lastSpanY/currentSpanY, view);
        return true;
    }

    /**
     * Event for end scaling by the user
     * @param focusX Focal point of scale X
     * @param focusY Focal point of scale Y
     * @param scaleFactor Current scale
     * @param lastSpanX Last span in x
     * @param lastSpanY Last span in y
     * @param currentSpanX Current span in x
     * @param currentSpanY Current span in y
     * @param view View to scale
     * @return True if consumed, false if not
     */
    public boolean onScaleEnd(float focusX,
                              float focusY,
                              float scaleFactor,
                              float lastSpanX,
                              float lastSpanY,
                              float currentSpanX,
                              float currentSpanY,
                              LineGraphView view) {
        return false;
    }

    /**
     * Handles a scroll gesture
     * @param originX origin of scroll in x
     * @param originY origin of scroll in y
     * @param distanceX Distance to handle in x
     * @param distanceY Distance to handle in y
     * @return True if consumed, false if not
     */
    public boolean onScroll(float originX, float originY, float distanceX, float distanceY, LineGraphView view) {
        return scroll(originX, originY, distanceX, distanceY, view);
    }

    /**
     * Handles a fling gesture
     * @param originX origin of scroll in x
     * @param originY origin of scroll in y
     * @param velocityX Fling velocity in X
     * @param velocityY Fling velocity in Y
     * @return True if consumed, false if not
     */
    public boolean onFling(float originX, float originY, float velocityX, float velocityY, LineGraphView view) {
        mScroller.forceFinished(true);
        if(view.displayToView(originX, originY, null)) {
            view.viewToDisplayDistance(0, 0, FLING_DISTANCE, FLING_DISTANCE, mFlingDistances);
            mScroller.fling((int) originX,
                            (int) originY,
                            (int) velocityX,
                            (int) velocityY,
                            (int) (originX - mFlingDistances.getX()),
                            (int) (originX + mFlingDistances.getX()),
                            (int) (originY - mFlingDistances.getY()),
                            (int) (originY + mFlingDistances.getY()));
            mFlingOrigin.setX(originX);
            mFlingOrigin.setY(originY);
            return true;
        }
        return false;
    }
}
