package com.biblo.datastreamviewer.linegraph.view.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import com.biblo.datastreamviewer.common.model.basic.PathLine;

/**
 * @author gchapman
 * Implements PathLine for android implementation, wrapper
 */
public class PathLineAndroid implements PathLine {
    private Path mLinePath = new Path();
    private Canvas mCanvas;
    private Paint mPaint;

    @Override
    public void clear() {
        mLinePath.reset();
    }

    @Override
    public void reset() {
        mLinePath.reset();
    }

    @Override
    public void add(float x, float y) {
        if(mLinePath.isEmpty()) {
            mLinePath.moveTo(x,y);
        }
        else {
            mLinePath.lineTo(x,y);
        }
    }

    @Override
    public void draw() {
        if(mCanvas != null || mPaint != null) {
            mCanvas.drawPath(mLinePath, mPaint);
        }
    }

    /**
     * Sets the canvas object to use for drawing
     * @param canvas Canvas to draw with
     */
    public void setCanvas(Canvas canvas) {
        mCanvas = canvas;
    }

    /**
     * @return Canvas object
     */
    public Canvas getCanvas() {
        return mCanvas;
    }

    /**
     * Set paint object for drawing path
     * @param paint Paint object for drawing path
     */
    public void setPaint(Paint paint) {
        mPaint = paint;
    }

    /**
     * @return Paint object
     */
    public Paint getPaint() {
        return mPaint;
    }
}
