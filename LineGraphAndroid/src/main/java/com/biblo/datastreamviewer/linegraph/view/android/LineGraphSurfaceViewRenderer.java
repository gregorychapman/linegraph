package com.biblo.datastreamviewer.linegraph.view.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;
import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.PathLine;
import com.biblo.datastreamviewer.common.model.basic.PropertyObject;
import com.biblo.datastreamviewer.common.model.basic.SimpleText;
import com.biblo.datastreamviewer.linegraph.model.MultiPointLine;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.view.LineGraphRenderer;

/**
 * @author gchapman
 * Will implement the surface view specific portions of the LineGraphRenderer.
 * This renderer should only be called on one thread (renderer loop)
 */
public class LineGraphSurfaceViewRenderer extends LineGraphRenderer {

    private static final String TAG = LineGraphSurfaceViewRenderer.class.getSimpleName();
    private static final int PROPERTY_LINE_PAINT = 0;
    private static final int PROPERTY_TEXT_PAINT = 1;
    private static final int PROPERTY_TEXT_SPACING = 2;

    private final SurfaceHolder mSurfaceHolder;
    private Canvas mCanvas;
    private Paint mBackground;

    private Paint mPaintLineDefault;
    private Paint mTextPaintDefault;
    private Float mTextSpacingDefault;

    /**
     * Constructs a renderer using a surface from Android
     * @param presenter Presenter of MVP
     * @param holder Surface holder containing the Android view surface
     */
    public LineGraphSurfaceViewRenderer(LineGraphPresenter presenter, SurfaceHolder holder) {
        super(presenter);

        mSurfaceHolder = holder;
        mBackground = new Paint();
        mBackground.setColor(Color.BLACK); //default black

        mPaintLineDefault = new Paint();
        mPaintLineDefault.setStrokeWidth(1);
        mPaintLineDefault.setAntiAlias(true);
        mPaintLineDefault.setColor(Color.LTGRAY);
        mPaintLineDefault.setAlpha(0x60);

        mTextPaintDefault = new Paint();
        mTextPaintDefault.setColor(Color.LTGRAY);
        mTextPaintDefault.setAlpha(0x60);
        mTextPaintDefault.setTextSize(25);
        mTextPaintDefault.setTextAlign(Paint.Align.LEFT);

        mTextSpacingDefault = 10f;
    }

    @Override
    public void addLineProperties(PropertyObject object, int color, float width) {
        Paint paint = (Paint) object.getProperty(PROPERTY_LINE_PAINT);
        if(paint == null) {
            paint = new Paint();
        }
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setStrokeWidth(width);
        paint.setStyle(Paint.Style.STROKE);
        object.putProperty(PROPERTY_LINE_PAINT, paint);
    }

    @Override
    public void addTextProperties(AttributableText textObject, int color, int size, String font,
                                  boolean bold, int alpha, int spacing, boolean centered) {
        Paint paint = (Paint) textObject.getProperty(PROPERTY_TEXT_PAINT);
        if(paint == null) {
            paint = new Paint();
        }
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setTextAlign(centered ? Paint.Align.CENTER : Paint.Align.LEFT);
        paint.setTypeface(Typeface.create(font, bold ? Typeface.BOLD : Typeface.NORMAL));
        paint.setTextSize(size);
        paint.setAlpha(alpha);
        textObject.putProperty(PROPERTY_TEXT_PAINT, paint);
        textObject.putProperty(PROPERTY_TEXT_SPACING, Float.valueOf(spacing));
    }

    @Override
    protected void drawLine(MultiPointLine line) {
        final Paint linePaint = (Paint) line.getProperty(PROPERTY_LINE_PAINT);
        if (linePaint == null) { //must be specified
            return;
        }
        PathLineAndroid path = (PathLineAndroid)mLinePath;
        path.setPaint(linePaint);
        path.setCanvas(mCanvas);
        super.drawLine(line);
    }

    @Override
    protected PathLine createLine() {
        return new PathLineAndroid();
    }

    @Override
    protected void drawLine(float startX, float startY, float endX, float endY, PropertyObject owner) {
        Paint linePaint = (Paint) owner.getProperty(PROPERTY_LINE_PAINT, mPaintLineDefault);
        mCanvas.drawLine(startX, startY, endX, endY, linePaint);
    }

    @Override
    protected void drawText(SimpleText text, PropertyObject owner) {
        Paint textPaint = (Paint) owner.getProperty(PROPERTY_TEXT_PAINT, mTextPaintDefault);
        Float textSpacing = (Float) owner.getProperty(PROPERTY_TEXT_SPACING, mTextSpacingDefault);

        //Manually scale, translate
        float scaleX = getScaleX();
        float scaleY = getScaleY();
        float pointX = getScreenX(text.getPoint().getX(), scaleX) + textSpacing;
        float pointY = getScreenY(text.getPoint().getY(), scaleY) - textSpacing;

        mCanvas.drawText(text.getText(), pointX, pointY, textPaint);
    }

    @Override
    protected void drawScreenText(SimpleText text, PropertyObject owner) {
        Paint textPaint = (Paint) owner.getProperty(PROPERTY_TEXT_PAINT, mTextPaintDefault);
        Float textSpacing = (Float) owner.getProperty(PROPERTY_TEXT_SPACING, mTextSpacingDefault);
        mCanvas.drawText(text.getText(),
                text.getPoint().getX() + textSpacing,
                mDisplayPort.getTop() - text.getPoint().getY() - textSpacing,
                textPaint);
    }

    @Override
    protected boolean initRender() {
        return true;
    }

    @Override
    protected boolean beginRender() {
        mCanvas = mSurfaceHolder.lockCanvas();
        if(mCanvas != null) {
            //NOTE: we assume that the view is full screen, in the case that it isn't
            //you must scale, translate, clip to the mDisplayPort using screen metrics
            mCanvas.drawPaint(mBackground);
            return true;
        }
        return false;
    }

    @Override
    protected void endRender() {
        if(mCanvas != null) {
            mSurfaceHolder.unlockCanvasAndPost(mCanvas);
            mCanvas = null;
        }
    }

    @Override
    protected void finishRender() {
    }

    @Override
    protected long getTimeNow() {
        return SystemClock.elapsedRealtime();
    }

    @Override
    protected void error(String message) {
        Log.e(TAG, message);
    }
}
