package com.biblo.datastreamviewer.linegraph.view.android;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.biblo.datastreamviewer.common.model.ScaleAuditor;
import com.biblo.datastreamviewer.common.model.basic.AttributableText;
import com.biblo.datastreamviewer.common.model.basic.Point;
import com.biblo.datastreamviewer.common.model.basic.PropertyObject;
import com.biblo.datastreamviewer.common.model.basic.Rectangle;
import com.biblo.datastreamviewer.common.model.WorldAuditor;
import com.biblo.datastreamviewer.common.model.basic.WritablePoint;
import com.biblo.datastreamviewer.common.model.basic.WritableRectangle;
import com.biblo.datastreamviewer.linegraph.presenter.LineGraphPresenter;
import com.biblo.datastreamviewer.linegraph.view.LineGraphView;

/**
 * @author gchapman
 * Implements the view that renders the line graph.
 */
public class LineGraphSurfaceView extends SurfaceView implements SurfaceHolder.Callback, LineGraphView {

    private final static String TAG = LineGraphSurfaceView.class.getSimpleName();
    private final static float MIN_DIFFERENCE = 0.01f;

    private final SurfaceHolder mSurfaceHolder;
    private final WritablePoint mScreenMetrics;
    private final WorldAuditor mWorldCheck;
    private final ScaleAuditor mScaleCheck;
    private LineGraphPresenter mPresenter;
    private LineGraphSurfaceViewRenderer mRenderer;
    private LineGraphGestureInput mGestureInput;
    private WritableRectangle mWorldBounds; //world coordinates
    private WritableRectangle mViewPort;    //world coordinates
    private WritableRectangle mDisplayPort; //pixels
    private boolean mRunning;
    private boolean mSurfaceCreated;
    private boolean mPendingRun;

    /**
     * Constructs an Android SurfaceView to render the line graph
     * @param context Android context that owns the view
     * @param metrics Use the metrics for screen information
     */
    public LineGraphSurfaceView(Context context, DisplayMetrics metrics) {
        super(context);
        mScreenMetrics = new WritablePoint(metrics.widthPixels, metrics.heightPixels);
        mSurfaceHolder = getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceCreated = false;
        mRunning = false;
        mPendingRun = false;
        mDisplayPort = new WritableRectangle();
        mWorldBounds = new WritableRectangle();
        mWorldCheck = new WorldAuditor(mWorldBounds);
        mScaleCheck = new ScaleAuditor(MIN_DIFFERENCE);

        //TODO store/load view port 330 144
        mViewPort = new WritableRectangle(-100,-100, 10000, 502);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        //bottom-up
        mDisplayPort.set(
                getPaddingLeft(),
                getHeight() - getPaddingTop(),
                getWidth() - getPaddingRight(),
                getPaddingBottom());
    }

    @Override
    public void startRender() {
        mPendingRun = !mSurfaceCreated;
        if(mSurfaceCreated && !mRunning) {
            mRenderer.start();
            mRunning = true;
        }
    }

    @Override
    public void stopRender() {
        mRenderer.stop();
        mRunning = false;
    }

    @Override
    public void setPresenter(LineGraphPresenter presenter) {
        mPresenter = presenter;
        mGestureInput = new LineGraphGestureInput(getContext(), mPresenter);
        mRenderer = new LineGraphSurfaceViewRenderer(mPresenter, mSurfaceHolder);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mGestureInput.onTouchEvent(event) || super.onTouchEvent(event);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mSurfaceCreated = true;
        if(mPendingRun) { //if startRender was called before surface created, run
            mRenderer.init();
            startRender();
            mPendingRun = false;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mSurfaceCreated = false;
        stopRender();
    }

    @Override
    public void addLineProperties(PropertyObject object, int color, float width) {
        mRenderer.addLineProperties(object, color, width);
    }

    @Override
    public void addTextProperties(AttributableText textObject, int color, int size, String font,
                                  boolean bold, int alpha, int spacing, boolean centered) {
        mRenderer.addTextProperties(textObject, color, size, font, bold, alpha, spacing, centered);
    }

    @Override
    public void update(long deltaTimeMillis) {
        mGestureInput.update(deltaTimeMillis, this);
        mWorldCheck.update(mViewPort);
        mScaleCheck.update(mViewPort);
    }

    @Override
    public Rectangle getViewPort(WritableRectangle viewPort) {
        viewPort.set(mViewPort);
        return viewPort;
    }

    @Override
    public Rectangle getDisplayPort(WritableRectangle displayPort) {
        displayPort.set(mDisplayPort);
        return displayPort;
    }

    @Override
    public void setWorldBounds(Rectangle worldBounds) {
        mWorldBounds.set(worldBounds);
    }

    @Override
    public void setViewPort(float left, float top, float right, float bottom) {
        mViewPort.set(left, top, right, bottom);
    }

    @Override
    public void setViewPort(float left, float bottom) {
        mViewPort.setX(left);
        mViewPort.setY(bottom);
    }

    @Override
    public boolean displayToView(float px, float py, WritablePoint viewPoint) {
        py = mDisplayPort.getHeight() - py;
        float pRatioX = (px - mDisplayPort.getLeft()) / mDisplayPort.getWidth();
        float pRatioY = (py - mDisplayPort.getBottom()) / mDisplayPort.getHeight();
        if(viewPoint != null) {
            viewPoint.setX(mViewPort.getLeft() + mViewPort.getWidth() * pRatioX);
            viewPoint.setY(mViewPort.getBottom() + mViewPort.getHeight() * pRatioY);
        }
        return mDisplayPort.contains(px, py);
    }

    @Override
    public boolean viewToDisplay(float x, float y, WritablePoint displayPoint) {
        float vRatioX = (x - mViewPort.getLeft()) / mViewPort.getWidth();
        float vRatioY = (y - mViewPort.getBottom()) / mViewPort.getHeight();
        float px = mDisplayPort.getLeft() + mDisplayPort.getWidth() * vRatioX;
        float py = mDisplayPort.getBottom() + mDisplayPort.getHeight() * vRatioY;
        if(displayPoint != null) {
            displayPoint.setX(px);
            displayPoint.setY(py);
        }
        return mDisplayPort.contains(px, py);
    }

    @Override
    public boolean displayToViewDistance(float originX, float originY, float spanX, float spanY, WritablePoint span) {
        if(span != null) {
            span.setX((spanX / mDisplayPort.getWidth()) * mViewPort.getWidth());
            span.setY((spanY / mDisplayPort.getHeight()) * mViewPort.getHeight());
        }
        return displayToView(originX, originY, null);
    }

    @Override
    public boolean viewToDisplayDistance(float originX, float originY, float spanX, float spanY, WritablePoint span) {
        if(span != null) {
            span.setX((spanX / mViewPort.getWidth()) * mDisplayPort.getWidth());
            span.setY((spanY / mViewPort.getHeight()) * mDisplayPort.getHeight());
        }
        return viewToDisplay(originX, originY, null);
    }

    @Override
    public Point getScreenResolution() {
        return mScreenMetrics;
    }

    @Override
    public boolean onDown() {
        return mGestureInput.onDown(this);
    }

    @Override
    public boolean onDoubleTap(float x, float y) {
        return mGestureInput.onDoubleTap(x, y, this);
    }

    @Override
    public boolean onScaleBegin(float focusX,
                                float focusY,
                                float scaleFactor,
                                float lastSpanX,
                                float lastSpanY,
                                float currentSpanX,
                                float currentSpanY) {
        return mGestureInput.onScaleBegin(focusX,
                focusY,
                scaleFactor,
                lastSpanX,
                lastSpanY,
                currentSpanX,
                currentSpanY,
                this);
    }

    @Override
    public boolean onScale(float focusX,
                           float focusY,
                           float scaleFactor,
                           float lastSpanX,
                           float lastSpanY,
                           float currentSpanX,
                           float currentSpanY) {
        return mGestureInput.onScale(focusX,
                focusY,
                scaleFactor,
                lastSpanX,
                lastSpanY,
                currentSpanX,
                currentSpanY,
                this);
    }

    @Override
    public boolean onScaleEnd(float focusX,
                              float focusY,
                              float scaleFactor,
                              float lastSpanX,
                              float lastSpanY,
                              float currentSpanX,
                              float currentSpanY) {
        return mGestureInput.onScaleEnd(focusX,
                focusY,
                scaleFactor,
                lastSpanX,
                lastSpanY,
                currentSpanX,
                currentSpanY,
                this);
    }

    @Override
    public boolean onScroll(float originX, float originY, float distanceX, float distanceY) {
        return mGestureInput.onScroll(originX, originY, distanceX, distanceY, this);
    }

    @Override
    public boolean onFling(float originX, float originY, float velocityX, float velocityY) {
        return mGestureInput.onFling(originX, originY, velocityX, velocityY, this);
    }
}
